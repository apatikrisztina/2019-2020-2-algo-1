# Info

* Minden "határidő"-t értsünk úgy, hogy a megadott nap 23:59 időpontjáig adható le a kész munka
* A megoldást a Canvas felületen adjátok le (méltányolható okokból lehet kivétel, ekkor emailben kérem)
* A határidőn túl beadott megoldásokat is átnézem, Canvasban ezeket is értékelem szövegesen - tanulni lehet belőle -, de arra pontot már nem adok
* Hacsak más nincs mondva, minden feladat 1 pontot ér (de ez Canvason is látszik)

# 02_BevezetoPeldak témakörhöz

## 01_BubbleSortCserek

* Határozzuk meg a buborékrendezés tanult verziójára az mswap<sub>bubbleSort</sub>, az Mswap<sub>bubbleSort</sub> és az Aswap<sub>bubbleSort</sub> értékeket - mindhárom helyes: 1 pont
* Bizonyítsuk is be a fentieket (lehet "esszé-jellegű", de legyen pontos, alapos) - mindhárom helyes: 1 pont
* Határidő: 2020. 04. 18.

## 02_BubbleSortJavitas

* Írd meg a buborékrendezés hatékonyabb verzióját az alábbi elgondolást követve:
    * 0-tól indexelt tömböt használj, az órán tanult algoritmusból indulj ki
    * Ha az adott külső körben az utolsó néhány vizsgált elem körében már nem volt csere, akkor azok az elemek egymáshoz képest sorrendben vannak. De az is biztos, hogy ezek az elemek nagyobbak minden őket megelőző elemnél, hiszen ha nem így lett volna, egy ilyen nagyobb elem felbuborékoltatott volna az utolsó elemek közé (netán utánuk)
    * Az órán tanult változat invariánsa csak annyit mondott: mindig egy elemmel több a már biztosan rendezett résztömb. Ez a verzió ennél többet tud, következésképpen hatékonyabb... mennyivel is?
* 1 pont jár a helyes struktogramért
* Még egy pont jár, ha megadod az m, M, A értékeket a compare (összehasonlítás) domináns műveletre, rövid szöveges érveléssel
* (A progalapos diasor nyújthat kis segítséget)
* Határidő: 2020. 04. 18.

## 03_LegendreSzorzas

* Határozzuk meg a tanult Legendre-algoritmus alapján az mmult<sub>legendre</sub>(a, k), Mmult<sub>legendre</sub>(a, k) és Amult<sub>legendre</sub>(a, k) értékeket (azaz a szorzások számát)
* Indokoljunk is meg a fentieket konkrét a-kkal, k-kkal, levezetve, hogy jött ki a válasz
* Az algoritmusban látható osztás nem számít szorzásnak, csak a "csillag"-gal jelölt műveletet számoljuk
* Határidő: 2020. 04. 18.

# 03_Muveletigeny témakörhöz

## 04_ThetaEkvRel

* Bizonyítsuk be, hogy a Θ-viszony ekvivalenciareláció, azaz:
    * reflexív
    * tranzitív
    * szimmetrikus
* Határidő: 2020. 04. 18.

## 05_OrdoOmegaEkvRel

* Vajon ekvivalenciareláció-e Ο és Ω? Döntsük el, és bizonyítsuk!
* Határidő: 2020. 04. 18.

# 04_LancoltListak témakörhöz

## 06_TorlesSL

* Írjuk meg a delete(&l : E1*, k : T) : L függvényt
    * l egy egyszerű láncolt lista (S1L), ami rendezett
    * Törölje a k kulcs első előfordulását tartalmazó E1-et (memóriaszivárgásra figyelve)
    * Térjen vissza egy boollal, ami igaz, ha sikerült a törlés (azaz volt k kulcsú elem)
    * Nyilván a törlést megelőzi egy keresés, ezt rátok bízom milyen módszerrel, hány pointerrel oldjátok meg
* Határidő: 2020. 05. 02.

## 07_TorlesHL

* Írjuk meg a delete(f : E1*, k : T) : L függvényt
    * f egy fejelemes láncolt lista (H1L), ami rendezett
    * Törölje a k kulcs első előfordulását tartalmazó E1-et (memóriaszivárgásra figyelve)
    * Térjen vissza egy boollal, ami igaz, ha sikerült a törlés (azaz volt k kulcsú elem)
    * Nyilván a törlést megelőzi egy keresés, ezt rátok bízom milyen módszerrel, hány pointerrel oldjátok meg
* Határidő: 2020. 05. 02.

## 08_Beszuras2L

* Írjuk meg az insert(&l : E2*, q : E2*) függvényt
    * l egy fejelem nélküli, rendezett kétirányú lista (S2L)
* Tipp: Nem kell két pointer a kereséshez, hiszen van visszairányú mutatónk
    * Viszont azt sem szabad elfelejteni frissíteni!
* Határidő: 2020. 05. 02.

## 09_TorlesH2L

* Írjunk függvényt, ami egy kétirányú, fejelemes listából törli a megadott kulcsú elemet
* A rendezettség rátok bízva
* A visszatérési típus és annak értelmezése is
* Határidő: 2020. 05. 02.

## 10_BeszuroRendezes

* Írjuk meg az insertionSort algoritmust egyszerű listára (S1L)
* Határidő: 2020. 05. 02.

## 11_Metszet

* Írjuk meg az intersect(f1 : E1*, f2 : E1*) függvényt
    * Az input két rendezett HL (fejelemes lista)
    * Az f1-be az algoritmus futásának végére kerüljön az eredeti f1 és f2 metszete (halmaz értelemben)
    * f2 pedig ne változzon (immutability)
    * A műveletigény legyen lineáris
* Határidő: 2020. 05. 09.

# 05_Vermek témakörhöz

## 12_Zarojelezes

* Írjuk meg a checkParentheses(s : InStream) : L függvényt
    * A bemenet egy olyan input folyam, ami egy s eleme { '{', '}', '[', ']', '(', ')' }* szót reprezentál
    * Azaz egy szóban a három féle zárójel karakterei lehetnek
    * A zárójel-fajták között az alábbi precedenciasorrend van:
        * {}-ben minden lehet
        * []-ben csak [] és ()
        * ()-ben csak ()
    * Minden zárójel párban kell legyen
    * 3 db számlálóval, verem meg egyebek nélkül döntsük el, hogy helyesen van-e zárójelezve egy ilyen szöveg
* Határidő: 2020. 05. 23.

## 13_Rendezes2Veremmel

* Írjuk meg a konzultáción hallott (és az ottani fájlban részletezett) "Rendezés két veremmel" feladat algoritmusát
* Határidő: 2020. 05. 23.

## 14_Nevsorforditas

* Írjuk meg a konzultáción hallott "Névsorfordítás" algoritmust az alábbi változatban:
    * Input: vesszőkkel elválasztott, kettőskereszttel terminált, szavakat tartalmazó szöveg, mint stringstream
    * Output: ugyanilyen formátumban de fordított sorrendben
    * Példa: in := Andras,Bea,Csilla,..,Zeno# -> out := Zeno,...,Csilla,Bea,Andras#
    * Stratégia
        * Két veremmel
        * Bedobok mindent s1-be
        * Miután kész vagyok ezzel, végigmegyek s1 elemein és átpakolom s2-be, de úgy, hogy amikor elválasztójel jön, akkor mindig kiírok s2-ből
* Határidő: 2020. 05. 23.

# 06_Sorok témakörhöz

## 15_SorImplementacio

* Adj meg egy típust reprezentációval és műveletimplementációkkal, ami a Queue interfészt megvalósítja (plusz legyen egy konstans műveletigényű hosszlekérő függvény)
    * A reprezentáció legyen egy nem végelemes lista, ill. tartalmazzon egy "length" attribútumot is
    * Most nem elvárás a feltétlen konstans műveletigény mindenre
* Határidő: 2020. 05. 23.

## 16_LNKO

* Írjuk meg a legnagyobb közös osztót kiszámoló programot
    * Bemenetként adott két szám prímtényezős felbontása egy-egy monoton növő sorban
    * Állítsuk elő egy harmadik sorba a két szám lnko-ját
    * Összefuttatásos algoritmus legyen, de mindegy, hogy unió- vagy metszet-alapú
    * Mindegy, hogy q1 vagy q2 vagy mindkét paraméter immutable, de az egyik legalább legyen az
* Határidő: 2020. 05. 23.

## 17_Pascal

* Írjuk meg a pascal(n : N) : Queue fejlécű algoritmust
    * Adja vissza n pozitív egész függvényében a Pascal-háromszög n-edik sorát egy Queue-ként
    * Pl.: pascal(5) -> <1, 4, 6, 4, 1>
* Határidő: 2020. 05. 23.
