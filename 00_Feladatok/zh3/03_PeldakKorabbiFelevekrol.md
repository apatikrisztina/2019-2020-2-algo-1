# Második zh

## Ihlet korábbi félévek zh-iból

* A 00_Feladatok/00_KorabbiFelevek mappába feltöltött elemek közül az alábbiak az alábbi megkötésekkel vonatkoznak az idei második zh tematikájára

### 2017.11.23. - első zh

* Fájlok:
    * zh1_20171123_fa.pdf
    * zh1_20171123_meo.pdf
* Vonatkozó feladat:
    * 3.
* Megjegyzések:
    * Verem ~ Stack
    * teto() ~ top()
    * Az "fv" jelöléssel adtuk meg, hogy függvény (nem pedig eljárás, aminek nem lenne return értéke), manapság már rendesen a visszatérési típus kiírásával jelezzük ezt
    * Elem ~ E1
    * kulcs ~ key
    * mut ~ next

### 2018.11.26. - első zh

* Fájlok:
    * zh1_20181126_fa.pdf
    * zh1_20181126_meo.pdf
* Vonatkozó feladatok:
    * 1. feladatban szerepel a lengyelformára hozás műveletigénye
    * 3.
    * 4.
* Megjegyzések:
    * Régen nagy L-lel jelöltük, amit most már kis l-lel
    * Verem ~ Stack
    * B ~ L (logikai típusú változó)

### 2019.05.08. - első zh

* Fájlok:
    * zh1_20190508_fa.pdf
    * zh1_20190508_meo.pdf
* Vonatkozó feladatok:
    * 2.
    * 3.
* Megjegyzések:
    * Mindkettő elég érdekes feladat, érdemes átfutni

### 2019.05.15. - második zh

* Fájlok:
    * zh2_201905015_fa.pdf
    * zh2_20190515_meo.pdf
* Vonatkozó feladatok:
    * 1.
* Megjegyzések:
    * A b) megoldást is érdemes átfutni (mínusz a fás részek), mert ott egy másik megoldást mutatok ugyanarra a feladatra