# 2020-as második zh

* A zh-t online (canvas) írtuk 2020. 04. 21-én 16.00 és 16.35 között 30 perces időkerettel
    * (1) Mindenki kapott egy 10 pontos láncolt listás kódolós feladatot - választhatott, hogy struktogrammal vagy pszeudokóddal akarja beadni
    * (2) Mindenki kapott egy 5 pontos algoritmusjavítós feldatot, a hibák számára és típusára a feladat szövege utalt
    * (3) Végül mindenki kapott egy szintén 5 pontos feladatot, ahol két kérdésre kellett válaszolni indoklással
* A zh a rendszer szerint 30 pontos volt, de mivel a struktogram és a pszeudokód vagy-vagy viszonyban volt egymással, így igazából 20 pontos volt
* A feladatokat random osztotta a rendszer, az alábbiakban feladatcsoportonként megadom az összes lehetséges feladatot és azok megoldásait
* Az egyes hallgatóknál a canvason előfordul, hogy viszonylag szűkszavúan értékeltem - a több helyen előforduló hibákról itt tudtok majd olvasni

## Első feladatcsoport - Kódolós

### Első lehetséges variáció

#### Feladat

* Írj algoritmust, ami egy paraméterként megadott tömböt és egy vele azonos elemtípusú egyszerű láncolt listát hasonlít össze. Akkor térjen vissza igazzal, ha a tömb 0. indexén ugyanaz az elem szerepel, mint a láncolt lista utolsó elemének kulcsa, a tömb 1. indexén az az elem, ami a láncolt lista utolsó előtti elemének a kulcsa, stb., tehát a láncolt lista fordított sorrendben tartalmazza a tömb elemeit. Minden más esetben legyen hamis a visszatérési érték. Törekedjünk a minimális műveletigényre.

#### Megoldás

* A feladat kissé gonoszan volt megfogalmazva, a lényeg az, hogy a tömbön jó műveletigénnyel végig tudunk menni bármilyen sorrendben, de a láncolt listát hasonlóan kedvező műveletigénnyel csak előrefelé tudjuk bejárni
* Azaz, ha a "láncolt lista fordított sorrendben tartalmazza a tömb elemeit", akkor a tömb is fordított sorrendben tartalmazza a láncolt lista elemein: a láncolt listán menjünk végig előrefelé és a tömbön visszafelé
* Több mindenre kell figyelnünk
    * Mehetünk úgy, hogy egy for ciklussal bejárjuk a tömböt, a ciklusmagban mindig növeljük p-t, és csekkoljuk persze, nem NULL-e. A végén pedig épp azt hogy NULL-e (különben nem lenne azonos a méretük)
    * Csinálhatnánk azt is, hogy a listát járjuk be és a ciklusmagban kezeljük a tömbben való tovább (vagyis vissza-)lépést
    * Mi végül a kettőt "egyszerre" csináljuk, így:

```
reverseEquals(a : T[n], l : E1<T>*) : L
  p := l
  i := n-1
  AMÍG p != NULL és i >= 0
    HA p->key != a[i]
      return false
    p := p->next
    i := i-1
  return p = NULL és i = -1
```

* Minden körben ellenőrzöm, hogy még mindkét bejáró értelmes helyen van-e, ha igen, mengézem azonosak-e az aktuális értékek; ha már nem, azt kell nézni egyszerre fogytak-e el
* Üres listára el se indulunk, éppúgy üres tömbre se
