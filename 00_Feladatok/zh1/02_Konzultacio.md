# Algoritmusok és adatszerkezetek 1., első zh konzultáció

* Helye: D 0-411
* Ideje: 2020. 03. 10. kedd, 18.00 – 18.30 

## 1. feladat - Futásiidő-becslés

* Egymillió elemű tömbökre futtattunk összefuttatásos és buborékrendezéseket. Az összefuttatásos rendezéseink futásidejeinek átlaga 4 másodperc volt. Adjunk egy becslést, mennyi lehetett a buborékrendezéseké! (logaritmustáblázat mellékelve: log<sub>2</sub>(1000000) ~ 20)
* Megoldás:
	* Tudjuk, hogy nagyságrendileg n * log<sub>2</sub>(n) a mergesort műveletigénye, ami itt kb. 10<sup>6</sup> * 20 = 2 * 10<sup>7</sup> db művelet. Azt is tudjuk, hogy ez volt 4 másodperc
	* A buborékrendezés műveletigénye n<sup>2</sup>, azaz esetünkben (10<sup>6</sup>)<sup>2</sup> = 10<sup>12</sup>
	* Ha most elosztjuk AT<sub>BS</sub>(10<sup>6</sup>)-t AT<sub>MS</sub>(10<sup>6</sup>)-nal, azaz 10<sup>12</sup>-t 2 * 10<sup>7</sup>-nel, akkor megkapjuk tehát, hogy a bubble sort hányszor volt lassabb, mint a mergesort, s mivel az 4 másodperc volt, azt kapjuk így meg, hányszor 4 másodpercig futottak kb. a buborékrendezéseink: 10<sup>5</sup> / 2
	* Tehát 10<sup>5</sup> / 2 = 50000 * 4 másodperc az időigény, ami 200000 s, ami 3333 és 1/3 perc, ami 55,55 óra, ami 2,3 nap

## 2. feladat - Műveletigény-osztályok

* Osszuk be a megfelelő osztályba, majd állítsuk nagyságrend szerinti növekvő sorrendbe az alábbi műveletigényeket leíró függvényeket. Ha akadnak nagyságrendileg azonos osztályba tartozó függvények is, ezt külön jelöljük
	* sqrt(n) + 5
	* move<sub>towerOfHanoi</sub>(n)
	* n * ln(n) + n<sup>1,1</sup>
	* n * ln(n) + n<sup>0,9</sup>
	* 10 * n<sup>3</sup> + 12 * n<sup>10/3</sup> – 130n + log(n)
* Megoldás:
	* A Hanoi tornyai feladatról tudjuk, hogy pontosan 2<sup>n</sup> - 1 átrakást jelent, ami miatt nagyságrendileg a Θ(2<sup>n</sup>) függvényosztályba tartozik
	* Az összes többi esetben a "szekvencia műveletigénye" tétel alapján a domináns tag műveletigényét kell néznünk
		* A gyökfüggvény meredekebben nő, mint a konstans (ami semennyire), így sqrt(n) + 5 eleme Θ(sqrt(n)), vagy más néven Θ(n<sup>1/2</sup>)
			* Látjuk, hogy tulajdonképpen ez egy hatványfüggvény. Azt kell megjegyeznünk, hogy a hatványfüggvények egymással nem ekvivalensek aszimptotikusan, mennél nagyobb a kitevő, annál erősebb a függvény. Az 1 alattiak ráadásul a lineárisnál is gyengébbek!
		* A következőnél az n<sup>1,1</sup> a meredekebb tag, és mint láttuk, a kitevő nem mindegy, ezért n * ln(n) + n<sup>1,1</sup> eleme Θ(n<sup>1,1</sup>)
		* Viszont az 1 alatti kitevő már gyengébb az n * log(n)-nél, ezért n * ln(n) + n<sup>0,9</sup> eleme Θ(n * log(n)), mivel a logaritmus alapja irreleváns
		* Végül itt is a legnagyobb kitevőjű tagot kell néznünk, ami trükkös módon nem az első: 12 * n<sup>10/3</sup>, de az együtthatókat elhagyhatjuk, ezért az utolsó kifejezés eleme Θ(n<sup>10/3</sup>)
	* A sorrend könnyedén előáll: sqrt(n) + 5 ≺ n * ln(n) + n<sup>0,9</sup> ≺ n * ln(n) + n<sup>1,1</sup> ≺ 10 * n<sup>3</sup> + 12 * n<sup>10/3</sup> – 130n + log(n) ≺ move<sub>towerOfHanoi</sub>(n)
		* Mivel nincs Θ-viszony egyik pár között sem, a sorban mindegyik függvény eleme az őt követő függvényre vett kis ordó halmaznak, példa: sqrt(n) + 5 eleme ο(n * ln(n) + n<sup>0,9</sup>)
		* Ha valahol ekvivalencia lett volna, az a Θ-kapcsolatot jelentené
		* Amit tudni kellett a sorbarendezéshez:
			* A hatványfüggvényeknél amelyik nagyobb kitevő, az nagyobb nagyságrend
			* Az n * log(n) nagyobb, mint bármelyik 1 alatti kitevőjű hatványfüggvény, és kisebb, mint bármelyik 1 feletti kitevőjű (és egyébként nagyobb, mint az n<sup>1</sup>-en, azaz a lineáris függvény)
			* A 2<sup>n</sup> mindenkinél sokkal nagyobb

## 3. feladat - Műveletigény-osztály bizonyítás

* Bizonyítsuk be a definícióból, hogy 0,5 * n<sup>3</sup> - n + 15 eleme Ο(n<sup>3</sup>)
* Megoldás:
	* Az Ο(n<sup>3</sup>) /kiolvasva nagy ordó/ egy halmaz, amibe azon függvények tartoznak, amelyikeknek n<sup>3</sup> aszimptotikus felső korlátja
	* A halmaz definíciója az alábbi: Ο(n<sup>3</sup>) := { g : N->R | van c>0 konstans és n<sub>0</sub> >= 0 küszöbindex, hogy minden n >= n<sub>0</sub>-ra: g(n) <= c * n<sup>3</sup> }
	* Ha az a kérdés, hogy 0,5 * n<sup>3</sup> - n + 15 egy ilyen "g"-e, akkor csak annyi a dolgunk, mutassunk megfelelő c-t és n<sub>0</sub>-t
		* Írjuk fel az egyenlőtlenséget: 0,5 * n<sup>3</sup> - n + 15 <= c * n<sup>3</sup>
		* Osszunk le n<sup>3</sup>-nal, ami máris maga után vonja, hogy n<sub>0</sub> legalább 1 (hogy tudjunk osztani - n természetes szám, ne feledjük): 0,5 - (1/n<sup>2</sup>) + (15/n<sup>3</sup>) <= c
		* Itt láthatjuk, hogy a bal oldal második és harmadik tagja ahogy n-t növeljük (márpedig növeljük, lásd definíció) egyre csak csökkenni fog, míg az első tag és a c nyilván erre érzéketlen
			* Tehát a bal oldal egyre közelebb lesz 0,5-höz..., ez n=1-re még 0,5 - 1 + 15, azaz 14,5, de utána egyre kisebb lesz, mivel a 3. tag gyorsabban csökken abszolút értékben, mint a második!
			* Azaz, ha megválasztjuk n<sub>0</sub>-t a "legrosszabb" esetnek, ami az 1, és erre is tudunk adni egy jó c-t, akkor nyert ügyünk van
			* De a c-t már meg is állapítottuk: legyen 14,5, arra igaz az egyenlőtlenség
	* Egyébként a Θ-viszony (Théta) is igaz lenne, de ez egy másik bizonyítás. Abból viszont mind a Ω (nagy omega), mind az Ο következik, de a o (kis ordó) és a ω (kis omega) éppen hogy nem!
    * Ilyen feladatoknál lehetőség szerint mindig mondjunk konkrét n<sub>0</sub>-t és c-t (konstruktív bizonyítás), vagy adjunk írásban egyértelmű érveket arra, hogy "kell lennie" ilyen számoknak
