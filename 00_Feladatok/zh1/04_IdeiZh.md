# 2020-as első zh

* A zh-t online (canvas) írtuk 2020. 04. 07-én 16.00 és 16.35 között 30 perces időkerettel
    * (1) Mindenki kapott egy 10 pontos tömbös kódolós feladatot - választhatott, hogy struktogrammal vagy pszeudokóddal akarja beadni
    * (2) Mindenki kapott egy 4 pontos a műveletigényekkel kapcsolatos fogalmakra rákérdező feleletválasztós feladatot
    * (3) És mindenki kapott egy 6 pontos műveletigény-osztályokba soroló, sorba rendezős feladatot
* A zh a rendszer szerint 30 pontos volt, de mivel a struktogram és a pszeudokód vagy-vagy viszonyban volt egymással, így igazából 20 pontos volt
* A feladatokat random osztotta a rendszer, az alábbiakban feladatcsoportonként megadom az összes lehetséges feladatot és azok megoldásait
    * Szeretném felhívni a figyelmet, hogy a 2. feladatnál automatikus értékelés volt, de ezt helyenként felülírtam:
        * Az egyik feladat hibás volt, ezt alább ki is emelem, arra mindenki megkapta a részpontját
        * Ha az "indoklós igaz-hamis" feladatnál az igaz-hamis értéket eltaláltad, de az indoklás rossz volt, adtam fél pontot, de ezt lefelé kerekítettem, tehát ha 2 ilyen volt, az ért 1 pontot
        * Végül, ha a többszörös igaz állítást kellett bejelölni, és nem tartottam igazságosnak az eredetileg a program által kiosztott részpontokat, ezen lehet, hogy növeltem

## Első feladatcsoport - Tömbös kódolós

### Első lehetséges variáció

#### Feladat

* Írj iteratív algoritmust, ami egy paraméterként megkapott, számokat tartalmazó, ```n``` méretű, ```a``` nevű tömbben meghatározza az inverziók számát (azaz megmondja hány olyan ```i```, ```j``` indexpár van, ahol ```i < j```, de ```a[i] > a[j]```)
* Törekedj a jó műveletigényre

#### Megoldás

* Itt rögzítettük a tömb nevét, elemtípusát (alapvetően Z-re, de R, Q, N is elfogadható akár), méretét. Ezeket kell tehát a struktogram fejében megadni
* Mivel ki kell valamit számolni, ami ráadásul egy darabszám, ezért természetes számmal térünk vissza
* Az "iteratív" azt jelenti, ciklust használjunk. Ezt azért kötöttem ki, mert van erre hatékonyabb rekurzív megoldás is, ami most nem volt a zh tárgya
* A lényeg az, hogy ne számoljunk kétszer semmivel (se hatékonyság, se a feladat szempontjából nem helyes)
    * A külső ciklusban az ```i```. elemeket vesszük sorra, ezek tehát amelyikek kisebb indexen lesznek, mint a ```j```. Mivel ```n-1``` az utolsó index, ```i = n-1```-hez már nem lesz nagyobb ```j``` index, ezért elég ```n-2```-ig menni
    * A belső ciklusban felsoroljuk az ```i```-nél nagyobb indexeket
* Természetesen lehet ezt fordítva is megközelíteni, ```i```-vel ```1```-től indulni ```n-1```-ig, ```j```-vel pedig az ```i```-nél kisebb indexű elemeket vizsgálni, de az elven ez nem változtat
* Figyeljünk a 0, esetleg 1 elemű tömb esetére, arra működik-e, mindig teszteljük le ezekre külön
    * Itt e kettő esetben 0-nak kell lenni az inverziók számának: és tényleg, hiszen be se lép a ciklusba
* Az se gond feltétlen, ha egy-két felesleges vizsgálatot is beveszünk, ha az üres tömb esetét nem rontja el (nem indexelünk ki) és az eredményt se számolja esetleg kétszer
    * Pl., ha véletlen ```i = j``` esetet is megengedsz, az funkcionálisan gondot nem okoz, mert ```a[i]``` ekkor nem lesz kisebb, mint ```a[j]```. Ha a feladat azt mondaná, hogy hány olyan elem van ```a[i]``` előtt, aminek az értéke azonos vele, akkor már nem lenne mindegy, hogy beleszámoljuk-e (a d) varició épp erről szól)

```
inversions(a : Z[n]) : N
    c := 0
    FOR i := 0 to n-2
        FOR j := i+1 to n-1
            HA a[j] < a[i]
                c := c+1
    return c
```

### Második lehetséges variáció

#### Feladat

* Írj algoritmust, ami egy természetes számokat tartalmazó,```b``` nevű tömböt kapván paraméterként, annak minden elemét felülírja a tömb legnagyobb értékű elemével
* Törekedj a jó műveletigényre

#### Megoldás

* A tömb neve ```b``` legyen, mert így volt rögzítve a feladatban
* Mivel ha természetes számok vannak benne, akkor a maximumuk is az, ezért végig ilyen számok lesznek benne, ez lesz a típusa
* Referencia jel nem kell, mert bár az értékei változnak, de maga a tömb nem
* Üres tömbnek nincs maximuma, ezért erre vigyázni kell, mivel természetes számokról van szó, ilyen esetben az is megoldás lehet, ha 0-ra inicializáljuk a maxot. De ez már Z-nél nem működött volna
* A végén a második FOR ciklus nem megúszható - hiszen az első végéig nem tudjuk mi a maximum
* A másodikban is lehet a ciklusváltozó ```i``` (de nem muszáj), mert az ilyen mindig csak a ciklus végéig él

```
overwriteWithMax(b : N[n])
	max := 0 (max : N segédváltozó)
	FOR i := 0 to n-1
		HA b[i] > max
			max := b[i]
	FOR i := 0 to n-1
		b[i] := max
```

* A hagyományosabb, általánosabb megoldás:

```
overwriteWithMax(b : N[n])
	HA n > 0
		max := b[0] (max : N)
		FOR i := 1 to n-1
			HA b[i] > max
				max := b[i]
		FOR i := 0 to n-1
			b[i] := max
```

### Harmadik lehetséges variáció

#### Feladat

* Írj algoritmust, ami egy paraméterként átadott, 1-től indexelt, ```c``` nevű, természetes számokat tartalmazó tömbről eldönti (ezzel térjen vissza), hogy az első felében levő, vagy a második felében levő elemek összege-e a nagyobb. Páratlan elemszámnál a középső elem ne tartozzon egyik "félbe" se.

#### Megoldás

* Az egytől indexelés, a ```c```, mint név, és az elemtípus itt is rögzített
* Boollal (L) kell visszatérnünk, ezt is jelezni kell az algoritmus fejében. Ha "visszatérést" mondok, akkor nem jó a referencia szerint átadott L típusú változó
* Értelemszerűen, ha ```1```-től indexelünk, az első fele ```1..n/2```-ig, a második fele ```(n/2)+1..n```-ig van
* Ha n páratlan, ```n/2``` a mi modellünkben tört szám lesz, ami nem alkalmas ciklus alsó/felső határnak
* Ezt ki lehet pl. így játszani:

```
isFirstHalfSumBigger(c/1 : N[n]) : L
    HA n % 2 = 0
        firstEnd := n/2
        secondStart := firstEnd + 1
    KÜLÖNBEN
        firstEnd := (n-1)/2 // pl. n=5 --> firstEnd = 2
        secondStart := firstEnd + 2 // pl. n=5 --> secondStart = 4
    c1 := 0
    FOR i := 1 to firstEnd
        c1 := c1 + c[i]
    c2 := 0
    FOR i := secondStart to n
        c2 := c2 + c[i]
    return c1 > c2
```

* ```i```, mint ciklusváltozó mindig csak a szülő ```FOR``` ciklusban él, ezért a neve "újrafelhasználható"
* A törtes probléma feloldására használhatjuk az ```alsóegészrész(n/2)``` kifejezést is, ami lefelé kerekíti ```n```-t (tehát páros ```n```-re ugyanazt adja, mint az ```n/2```)
    * Tisztességesen az "alsóegészrésznek" ez a jele: ```⸤n/2⸥```

```
isFirstHalfSumBigger(c/1 : N[n]) : L
    HA n % 2 = 0
        secondStart := n/2 + 1
    KÜLÖNBEN
        secondStart := alsóegészrész(n/2) + 2
    c1 := 0
    FOR i := 1 to alsóegészrész(n/2)
        c1 := c1 + c[i]
    c2 := 0
    FOR i := secondStart to n
        c2 := c2 + c[i]
    return c1 > c2
```

* Hasonlóan definiálhatjuk a ```felsőegészrész(n/2)```-t is (páros ```n```-re ez is a felét adja)
    * Jele, ha szépen akarunk írni: ```⸢n/2⸣```

```
isFirstHalfSumBigger(c/1 : N[n]) : L
    c1 := 0
    FOR i := 1 to alsóegészrész(n/2)
        c1 := c1 + c[i]
    c2 := 0
    FOR i := felsőegészrész(n/2) + 1 to n
        c2 := c2 + c[i]
    return c1 > c2
```

* Van ennél elegánsabb út is, elindulok az elejétől is és a végétől is, "párhuzamosan"

```
isFirstHalfSumBigger(c/1 : N[n]) : L
    c1 := 0
    c2 := 0
    FOR i := 1 to alsóegészrész(n/2)
        c1 := c1 + c[i]
        c2 := c2 + c[n-i+1]
    return c1 > c2
```

* Még két komment:
    * Ha a feladatban nem rögzítem, a tömb méretet nem muszáj a deklarációban megadni, de ekkor az ```n``` literált nem, csak a ```c.length```-et használhatjuk, de más jelentősége nincs
* A feladat megfogalmazása szerint az is jó megoldás, ha valaki nem booleannal, hanem az "első" vagy "második" felet azonosító pl. számmal tért vissza
    * Ekkor persze az algoritmus feje is változik

```
isFirstHalfSumBigger(c/1 : N[]) : N
    c1 := 0
    c2 := 0
    FOR i := 1 to alsóegészrész(c.length/2)
        c1 := c1 + c[i]
        c2 := c2 + c[c.length-i+1]
    HA c1 > c2
        return 1
    KÜLÖNBEN
        return 2
```

* Ebből esetleg lehet 3 ágú elágazás is, hogyha egyik oldal összege se nagyobb, akkor pl. 0-val térjen vissza

### Negyedik lehetséges variáció

#### Feladat

* Írj algoritmust, ami egy természetes számokat tartalmazó, ```d``` nevű, 1-től indexelt tömb minden elemét kicseréli egy olyan számra, ami megmondja, az adott elem hányadszorra fordult elő a tömbben
* Csak a tanult eszközöket használd
* Törekedj a jó műveletigényre
* pl: ```d = [1,2,1,3,2,1] --> d = [1,1,2,1,2,3]```

#### Megoldás

* Fontos, hogy ```d``` legyen a tömb neve, ha ez kitétel volt
* A további a típusát meghatározó pontokat se feledjük: 1-től indexelt, N-alapú
* Ha nem akarjuk felülírni a szükséges elemeket, hátulról előre kell haladnunk, esetleg segédtömböt kell készítenünk - ez az eset azért nem szerencsés, mert ekkor referencia szerint kell átadni, tehát az interfészen bonyolít, vagy utána még egy ciklust kell írnunk az elemenkénti másoláshoz
* Kint a vadonban ezt asszociatív tömbbel, hasheléssel oldanánk meg

```
replaceWithOrdinals(d/1 : N[n])
	FOR i := n down to 1
		c := 1
		FOR j := 1 to i-1
			HA d[i] == d[j]
				c := c+1
		d[i] := c
```

* Az ```i```. elemet nem vizsgáltam, mert úgyis tudom, hogy egyenlő saját magával az értéke. Emiatt kezdtem a számlálást ```1```-től
* Lehetne eleve ```d[i]```-t növelgetni, de akkor is ki kellett volna menteni az eredeti értékét egy változóba, hogy legyen mivel hasonlítgatni
* Segédtömbös megoldás:

```
replaceWithOrdinals(&d/1 : N[n])
	s/1 : N[n]
	FOR i := 1 to n
		s[i] := 1
		FOR j := 1 to i-1
			HA d[i] == d[j]
				s[i] := s[i]+1
	d := s
```

## Második feladatcsoport - Műveletigényes feleletválasztós

### Első lehetséges variáció

#### Feladat

* Igazak-e az alábbiak? Miért?
    * (1) 4n + 3n eleme Théta(7n)
        * **Igaz**, mert Théta ekvivalenciareláció
        * **Igaz**, mert a két tagból a 4n a domináns tag, az pedig csak konstans szorzóval tér el 7n-től
        * **Nem igaz**, mert így nem lehet kiemelni a szorzókat
        * **Nem igaz**, mert 4+3 ugyan 7, de itt szorozni kell
    * (2) n^(3/5) * n + n^(5/3) eleme Théta(n^(5/3))
        * **Igaz**, mert a 3/5 kisebb, mint 1, ezért a második tag a domináns
        * **Igaz**, mert az első tag ugyan a domináns, de a kitevő nem számít Thétailag
        * **Hamis**, mert az első tag a domináns, és ugyan a pontos kitevő nem számít, de az igen, hogy nagyobb-e egynél
        * **Hamis**, mert az első tag a domináns és számít a kitevő
    * (3) log_3(n) eleme Théta (n^(3/5) + log_2(n))
        * **Igaz**, mert  a Théta szimmetrikus, innentől lehet a domináns taggal számolni, ami a log, az alap pedig nem számít
        * **Hamis**, mert  bár a Théta szimmetrikus, innentől lehet a domináns taggal számolni, ami a log, de az alap számít
        * **Hamis**, mert  a Théta ugyan szimmetrikus, innentől lehet a domináns taggal számolni, de ez nem a log
        * **Igaz**, de nem a "Théta szimmetrikus" okokból.... definícióba helyettesítve viszont lesz n_0, c_1, c_2
    * (4) n eleme Nagy Ordó(1/2n)
        * **Hamis**, mivel az Ordó a "kisebbet" jelenti, tehát ha nem 1/2 lenne, hanem 1-nél nagyobb a szorzó, jó lenne
        * **Hamis**, mivel az Ordó a "kisebb-egyenlőt" jelenti, tehát ha nem 1/2 lenne, hanem 1-nél nagyobb-egyenlő a szorzó, jó lenne
        * **Hamis**, mert ez a Thétában van, emiatt nem lehet Ordó
        * **Igaz**, mert egyenlőség megengedett, és nem számít a konstans szorzó

#### Megoldás

* (1) - a)
    * Ha normálalakra hozzuk, látjuk ugyanaz van mindkét oldalon. Théta ekvivalenciareláció és annak része az ilyenkor alkalmazandó reflexivitás
    * Itt nincs két tag, mert igazából itt csak lineáris tag van, csak kettévéve
    * Igen is összeadni kell és így kell kiemelni
* (2) - ez volt a hibás
    * Az első tag igazából n^(8/5), ennél a második kitevője nagyobb, emiatt az a domináns
    * Számít a kitevő, a nagyobb kitevő a nagyobb nagyságrend
    * E szempontból végülis mindegy, hogy 1 feletti-e vagy sem - de az 1 alatti és feletti kitevősök közé befurakodik pl. az n*log(n) nagyságrendje
* (3) - c)
    * A Théta szimmetrikus, ezért ez ugyanaz, mintha azt néznénk: n^(3/5) + log_2(n) eleme Théta (log_3(n))
    * Itt viszont már használhatjuk a szekvencia tételét, azaz a domináns tagot kell néznünk
    * Azt tudni kell, hogy a log alapja nem számít, ill. hogy a log és az egy alatti kitevős hatvány is kicsi nagyságrend, de e kettőből a hatvány a nagyobb
    * Az meg egyértelmű, hogy n^(3/5) nem eleme Théta(log(n))
* (4) - d)
    * A nagy Ordó a kisebb-egyenlőt jelenti, de konstans szorzótól és eltolástól függetlenül
    * Attól hogy valami Théta még lehet Ordó, sőt egyenesen következik is belőle

### Második lehetséges variáció

#### Feladat

* Jelöld be az összes igaz állítást az alábbiak közül!
    * (1) - Minden műveletigényt leíró függvényhez megadható egy nemüres kis ordó halmaz
    * (2) - Minden műveletigényt leíró f függvényhez mondható végtelen sok műveletigényt leíró függvény, ami az f Nagy Ordó halmazában van
    * (3) - Minden műveletigényt leíró függvényhez megadható egy nemüres Théta halmaz
    * (4) - Minden műveletigényt leíró függvényhez megadható egy nemüres Nagy Ordó halmaz

#### Megoldás

* (1) - Az azonosan 0-hoz nem adható meg
* (2) - Az azonosan 0-éban csak önmaga van
* (3) - Igen, mert reflexív
* (4) - Igen, mert reflexív

### Harmadik lehetséges variáció

#### Feladat

* Mindegyik állításhoz válaszd ki az egyetlen helyes választ
    * (1) - 33 és 1/3 eleme Omega(...)
    * (2) - 3n eleme Théta(...)
    * (3) - -3n + 33 és 1/3 eleme Ordó(...)
    * (4) - abszolútértékfüggvény eleme Théta(...)
* Lehetséges válaszok mindhez:
    * (a) - 4n
    * (b) - 420
    * (c) - Ezekből egyik se, de lenne olyan műveletigény-függvény
    * (d) - Nem létezik olyan műveletigény-függvény

#### Megoldás

* (1) - Ez egy konstans, az Omega azt jelenti, ami a zárójelben van, az kisebb-egyenlő mint ez, a 4n nyilván nagyobb-egyenlő, tehát marad a (b)
* (2) - Ez lineráis, akár az (a)
* (3) - Mivel a domináns tag negatív, ez nem is műveletigény-függvény, ezért (d)
* (4) - Az abszolútérték ugyan értelmezve lehetne negatív számokon is, de mi leszűkítjük ezt a definíciók alapján a természeres számokra (és ami fontosabb, a képe nem lehet negatív). Így minden n-hez önmagát rendeli, azaz lineáris, azaz (a)

## Harmadik feladatcsoport - Műveletigény-osztályos

### Első lehetséges variáció

#### Feladat

* Sorold műveletigény-osztályokba (a legegyszerűbb taggal reprezentálva) az alábbi műveletigény-függvényeket
* Mindnél legalább röviden indokolj
* Rendezd sorba a függvényeket, tedd ki közéjük az "Ordó", "Théta" "Omega" jeleket!
    * (1) - Lépésszám(Egy n méretű méretéről eldönteni páros szám-e)
    * (2) - Lépésszám(Egy n méretű tömb utolsó indexén levő számról eldönteni páros-e)
    * (3) - Lépésszám(Egy n méretű tömb páros eleinek számának meghatározása)
    * (4) - 3n*3log_3(n)
    * (5) - 3n*3log_3(n^3)
    * (6) - n*n^2 + (n^3)/2

#### Megoldás

* (1) - A méret konstans lekérhető, és a párosság eldöntése is az: Théta(1)
* (2) - Konkrét indexet lekérni konstans, az utolsó meghatározása a méretből szintén az: Théta(1)
* (3) - Végig kell menni az összesen: Théta(n)
* (4) - Konstans szorzó (összesen 9) nem számít, log alapja se, ez marad: Théta(n*log(n))
* (5) - 3. hatvány kiemelhető szorzónak, ezáltal ugyanazt kapjuk, mint az imént csak most a 27-es szorzó nem számít: Théta(n*log(n))
* (6) - n*n^2 = n^3, innen ez összevonható, szorzó elhagyható: Théta(n^3)
* Emelkedő sorrendben:
    * (1) ~ (2)
    * (3)
    * (4) ~ (5)
    * (6)

### Második lehetséges variáció

#### Feladat

* Sorold műveletigény-osztályokba (a legegyszerűbb taggal reprezentálva) az alábbi műveletigény-függvényeket
* Mindnél legalább röviden indokolj
* Rendezd sorba a függvényeket, tedd ki közéjük az "Ordó", "Théta" "Omega" jeleket!
    * (1) - Szorzások+ÖsszeadásokSzáma(Naiv polinomszámolás)
    * (2) - Lépésszám(n méretű tömb középső elemének kiírása)
    * (3) - Lépésszám(n méretű rendezett tömbben az "n" érték megkeresése)
    * (4) - log_2(n)
    * (5) - 2* log_n(n)
    * (6) - 3n + 3n^2 + 5n^3 - 6n^3

#### Megoldás

* (1) - Az egyik négyzetes, a másik lineáris, előbbi a domináns: Théta(n^2)
* (2) - Méret konstans, ezt felezni konstans, indexelni konstans, kiírni konstans: Théta(1)
* (3) - Rendezett inputon egy konkrét érték keresésére van a logaritmikus keresés: Théta(log(n))
* (4) - Az alap nem számít: Théta(log(n))
* (5) - Konstans szorzó nem számít, log_n(n) = 1: Théta(1)
* (6) - Ez egy polinom, ahol a domináns tag az n^3 aminek -1 végül is az együtthatója - emiatt ez nem műveletigény-függvény: átverés :(
* Emelkedő sorrendben:
    * (2) ~ (5)
    * (3) ~ (4)
    * (1)

### Harmadik lehetséges variáció

#### Feladat

* Sorold műveletigény-osztályokba (a legegyszerűbb taggal reprezentálva) az alábbi műveletigény-függvényeket
* Mindnél legalább röviden indokolj
* Rendezd sorba a függvényeket, tedd ki közéjük az "Ordó", "Théta" "Omega" jeleket!
    * (1) - Lépésszám(Egy n méretű tömb kiíratása)
    * (2) - Lépésszám(Egy n méretű tömb kiíratása fordítva)
    * (3) - ÖsszehasonlításokSzáma(Egy n méretű tömb buborékrendezése)
    * (4) - log_10(n) + n + n * log_10(3)
    * (5) - 2n^2 * 3n^3
    * (6) - 2n^2 + 3n^3

#### Megoldás

* (1) - Minden elemen végig kell menni: Théta(n)
* (2) - Fordítva se nem lassabb, se nem gyorsabb: Théta(n)
* (3) - Két egymásba ágyazott ciklus: Théta(n^2)
* (4) - A harmadik tag egy konstans, az első logos, a második lineáris - utóbbi a domináns: Théta(n)
* (5) - Ez egy tag, össze kell sozorzni: 6*n^5, konstans szorzó elhagyható, kitevő számít: Théta(n^5)
* (6) - Ez két tag, konstans szorzók elhagyhatók, amúgy a nagyobbik kitevő a domináns: Théta(n^3)
* Emelkedő sorrendben:
    * (1) ~ (2) ~ (4)
    * (3)
    * (6)
    * (5)
