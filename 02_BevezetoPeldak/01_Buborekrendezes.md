# Buborékrendezés tömbre

## Elv

* Legyen adott egy valamilyen rendezéssel rendelkező alaptípus feletti tömb. Az órai példa a karakter típus volt az ábécérend szerinti rendezéssel. Itt most az egész számok. A feladat az, hogy rendezzük növekvő sorba a tömb elemeit
* Az alapelgondolás a következő: végigmegyünk sorban az összes elemen, és ha ő és a rákövetkezője között inverzió van, megcseréljük őket

* Példa
	* a<sub>1</sub> = [3, 1, 2, 5, 4]
		* 1. kör: [3 1] 2 5 4 - csere
		* 2. kör: 1 [3 2] 5 4 - csere
		* 3. kör: 1 2 [3 5] 4 - nincs csere
		* 4. kör: 1 2 3 [5 4] - csere
		* Végeredmény: 1 2 3 4 5 - rendezett
	* Látható, hogy ha mindig a két bekeretezett elemet vizsgálom és ezt a "keretet" minden lépésben eltolom, végül ha pl. a legnagyobb elem volt az első indexen, ez az utolsó lépésre valóban a leghátsó indexre kerül. Ezt szokták "felbuborékoltatásnak" nevezni, innen a rendezés neve
	* De azért ez nem ilyen egyszerű, ha pl. az első három indexen levő elem között nincs ott az első három érték, máris nem elég csak ennyit tenni
* Példa
	* a<sub>2</sub> = [3, 1, 5, 2, 4]
		* 1. kör: [3 1] 5 2 4 - csere
		* 2. kör: 1 [3 5] 2 4 - nincs csere
		* 3. kör: 1 3 [5 2] 4 - csere
		* 4. kör: 1 3 2 [5 4] - csere
	* Most az utolsó már a helyén van, a többin újrakezdjük:
		* 1. kör: [1 3] 2 4 | 5 - nincs csere
		* 2. kör: 1 [3 2] 4 | 5 - csere
		* 3. kör: 1 2 [3 4] | 5 - nincs csere
	* Utolsó kettő a helyén, újra:
		* 1. kör: [1 2] 3 | 4 5 - nincs csere
		* 2. kör: 1 [2 3] | 4 5 - nincs csere
	* Utolsó 3 a helyén, újra:
		* 1. kör: [1 2] | 3 4 5
	* Utolsó 4 a helyén, ezzel kizárásos alapon a maradék 5. legnagyobb elem is a helyén, végeredmény:
		* 1 | 2 3 4 5
	* Itt nem volt elég az első példa naiv megközelítése. Mivel azt már láttuk, hogy egy futásra az egész tömb legnagyobb eleme felbuborékoltatott az utolsó helyre, ezért az első kör után mondhatjuk, hogy az utolsó indexből álló egy elemű résztömb már rendezett. Sőt ennél többet is mondhatunk: rendezett, és az egész tömböt tekintve a legnagyobb elemek vannak itt, vagy másképp fogalmazva, ezek az elemek már a majdani teljes rendezett tömböt tekintve a helyükön vannak
		* Érdemes ilyen invariáns (a ciklus minden lépése után fennálló) állításokat megfogalmazni, mert segíthet az algoritmust kitalálni, helyességét alátámasztani, elvét memorizálni
		* Itt tehát kimondhatjuk, hogy a ciklus invariánsa k eleme [0..n-1]-re, hogy az utolsó k elem már le van választva, azaz az utolsó k elemből álló résztömbben a k legnagyobb elem van és ezek egymáshoz képest rendezettek - azaz az utolsó k darab elem a végső helyén van
		    * Ez k = 0-ra az azonosan igaz - a k = 0 a "ciklus nulladik köre után"-t jelenti, azaz a ciklusba való belépés előtt igaz állítást
		    * k = n-1-re pedig azt, hogy az utolsó n-1 helyen megvan az n-1 legnagyobb elem, míg a még rendezetlen résztömb egy elemű, így önmagában rendezett (nem is tudnám rendezni, mert nincs rákövetkezője a nem leválasztott részben). Mivel mind az n-1 mögötte levő elem nagyobb nála, szükségszerűen ez az első elem a globális legkisebb, tehát a nulladik indexen levő elem is a helyén van
		* Az invariáns attól invariáns, hogy a ciklus minden köre megtartja, innen könnyen kitalálható a külső ciklus intervalluma: n-1 kört kell menni, mert ha "csak" az invariánst sikerül végig fenntartani, az utolsó körre kész is leszünk
        * Ha a ciklussal n-1-től megyünk 1-ig visszafelé egyesével (ami n-1 db kör), a ciklusváltozó meg tudja mondani az első a kör végére leválasztott legkisebb elem indexét. Azaz az első kör után egy elem kerül a helyére, ez lesz az i = n-1. Utána az utolsó 2, ez az i = n-2, stb. A végére pedig az utolsó n-1 db elem az i = n-(n-1), azaz i = 1
	* A belső ciklusban pedig az lesz a dolgunk, hogy páronként hasonlítsuk az elemeket, mégpedig úgy, hogy a leválasztott elemeket már ne vizsgáljuk
		* Ezért kell itt i-1-ig menni. Az i jelenti, hogy hol tartunk a külső ciklusban. Mivel most járunk i-nél, ezért a kör végére az i. elem már le lesz választva, de most még nincs. Viszont majd mindig a j. és a j+1. elemeket hasonlítjuk össze, azaz ha az a célunk, hogy az i. elem legyen a legnagyobb, amit vizsgálunk, akkor max. i-1-ig mehetünk (mert így lehet j+1 = i)
		* Ha i kezdetben n-1, akkor frankón kijön, hogy kezdetben mind az n elemet néznünk kell
	* Azt is láthatjuk, hogy ezt a két egymásba ágyazott ciklust addig kell ismételnünk, amíg el nem fogy a tömb kezdőszelete, mert addig nem lehetünk biztosak, hogy nincs inverzió. Azaz bár az a<sub>1</sub>-es példánál szabad szemmel is látható hogy már egy kör után is rendezetté vált, de természetesen az algoritmus pont ugyanannyi vizsgálatot fog megtenni ott is, mint az a<sub>2</sub> esetében, hogy ezt tényleg ki is merje mondani

## Algoritmus

* Az alábbi struktogrammal megadva:

```
bubbleSort(a : T[n])
    i := n-1 down to 1
        j := 0 to i-1
            HA a[j] > a[j+1]
                swap(a[j], a[j+1])
```

* Itt tehát a 0-tól n-1-ig van indexelve, így n db eleme van
* Ha a-t 1-től indexelnénk az alábbi dolgok változnának:

```
bubbleSort(a/1 : T[n])
    i := n down to 2
        j := 1 to i-1
            HA a[j] > a[j+1]
                swap(a[j], a[j+1])
```

* Vegyük észre, hogy a ciklusok lefutásának száma annak ellenére megegyezik a két változatban, hogy j a második verzióban alacsonyabb indexről indul, de ugyanaddig megy. Muszáj i-1-ig mennie, hiszen ha mindkét indexből egyet kivonunk, a köztük levő különbség ugyanannyi marad

## swap

* Mi lehet a swap (csere) formális paraméterlistája, szignatúrája?
	* Válasz: swap(&x : T, &y : T)
		* A referenciajelet a formális és csakis a formális paraméterlistában ki kell rakni, ha és csakis ha az átadott változónak értéket adunk s ezt "kifelé láttatni akarjuk"
		* Az "a[i]" alakú formális paraméternév nem lenne helyes, mert egyrészt az egy kifejezés, nem pedig egy most bevezetendő álnév-változó, másrészt meg azt sugallná, csak az "a" tömb elemei között valid elképzelés a csere. Pedig bármely két változó között értelmes ez a szándék - és szeretjük a dolgokat általánosan megfogalmazni
		* Mivel a egy T feletti tömb, ezért értelemszerűen x és y álnevekhez a T típus járul
		* Mivel a formális és az aktuális (itt most a[i], a[i+1]) paraméterlista között csak a paraméterek száma, sorrendje, típusa a megfeleltetés alapja, bárhogy hívhatjuk x-et a formális listában. Akár a-nak is, pedig az a meghívás helyén a tömböt jelentette, nem annak egy elemét
* A swap tehát cserél két elemet, ezt egy segédváltozó bevezetésével teszi, hiszen különben az egyik elemet elveszítenénk:

```
swap(&x : T, &y : T)
    a := x
    x := y
    y := a
```

* Ha megengedjük az ún. szimultán értékadást, ez így is megoldható:

```
swap(&x : T, &y : T)
    x,y := y,x
```

* Ha T szám, akkor segédváltozó nélkül is megoldható a feladat. Meggondolandó

## Hatékonyság

* Két egymásba ágyazott ciklusról van szó, a külső n-1 kört megy, a belső meg ahhoz képest megy annyit, amekkora a nem leválasztott rész mínusz egy
* Konkrét példa az összehasonlítások számára (azaz, hogy hány [] keretet írtam le)
	* compare<sub>bubbleSort</sub>(5) = 4 + 3 + 2 + 1 = 10
* Általánosan tehát: (n-1) + (n-2) + ... + 1 = (számtani sorozat - első+utolsó * tagok száma / 2) = [(1 + (n-1)) * n-1]/2 = [n(n-1)]/2 = (1/2)n<sup>2</sup> - (1/2)n
	* Azaz ez egy polinom. Erre szokták mondani, hogy polinomiális futási idő
	    * Mivel ez egy négyzetes polinom, ezért ezt konkrétabban négyzetes komplexitásnak fogjuk hívni később
	* Nyilván ez az összefüggés érvényes minden n méretű bemenetre, azaz mcompare<sub>bubbleSort</sub>(n) = Mcompare<sub>bubbleSort</sub>(n) = Acompare<sub>bubbleSort</sub>(n) s így: = compare<sub>bubbleSort</sub>(n)
* A cserék száma az eredeti tömbbeli inverziók számával azonos, és erre már nem igaz a fenti állítás, függ az inputtól a konkrét szám:
	* swap<sub>bubbleSort</sub>(a<sub>1</sub>) = 3
	* swap<sub>bubbleSort</sub>(a<sub>2</sub>) = 4
    * Az inverziók számát úgy lehet kiszámolni, hogy minden elemre megnézem, hány darab őt követő kisebb érték van, és ezeket összegzem. Konkrétan:
        * inv(a<sub>1</sub>) = 2 + 0 + 0 + 1 + 0
        * inv(a<sub>2</sub>) = 2 + 0 + 2 + 0 + 0

## Javított verzió

* Mint láthattuk, a belső ciklus minden körben egyre kisebb intervallumot jár be, mondván az "utolsó néhány" elemet leválasztottuk, helyére raktuk
    * Ez az invariáns, hogy mindig amennyi kört ment a külső ciklus, hátulról számolva annyi utolsó elem van a helyén
* De elképzelhető, hogy ennél már több elem is a helyén van, akár lehet eleve is rendezett a tömb
    * Ahhoz, hogy a rendezettséget ellenőrizzük, egyszer biztosan végig kell menni a tömbön
* Ha a tömb rendezett volt eleve, akkor egy cserét se futtatunk le. De mivel akkor ez a kör nem okozott cserét, akkor a következő körben is ugyanezeket az elemeket hasonlítjuk össze (igaz csak eggyel kevesebb párt nézünk), tehát biztos hogy akkor sem lesz csere, és így tovább
* Azaz, ha valaha a belső ciklus egy futásánál azt tapasztaltuk, hogy nem volt csere, a külső ciklussal le is állhatunk, mert már nem is lesz
    * Azokat az elemeket, amiket pedig nem érintettünk, az invariáns miatt nem is kellett
* Ennél többet is mondhatunk. Ha egy körben a vizsgálandó elemek körében a "néhány utolsó" elemet tekintve már nem volt csere, akkor azok a következő körben se fognak már cserélődni, tehát nem csak egyet, hanem ezt a néhány elemet is már úgy tekinthetjük, hogy a következő körre leválaszthatók
    * Hiszen nem volt csere, de a következő körben se lett, hiszen a "status quo"-t csak a csere tudná megváltoztatni
* Ennek szellemében tudunk javított algoritmust készíteni, aminek a struktogramja majd szorgalmi feladat lesz