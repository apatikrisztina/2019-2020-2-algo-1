# Infix forma átalakítása lengyelformára

* Nézzünk előbb két egyszerűbb példát, majd az algoritmust, végül egy összetettebb levezetést
* Az infix formában adott input: a + b * c - d
	* Vermet használunk, ha operandust látok csak leírom (hiszen azok sorrendje nem változik), ha operátort, akkor berakom a verembe
		* a-t leírom
		* \+ a verembe
		* b-t leírom
		* \* a verembe
		* c-t leírom
		* \- esetében mivel a veremtető nagyobb precedenciájú, ezért kiveszem azt s leírom; de még mindig van valami, ami ugyan "csak" azonos precedenciájú, de mivel balról jobbra köt, ezért ezt is kiveszem, leírom, majd mivel a verem már üres, berakhatom a mínusz jelet
		* d-t leírom
		* Mivel elfogyott a bemenet, mindent ami a veremben van, kiírom (-)
	* Eredmény: a b c * + d -
	* Ellenőrizzük le, ez valóban lengyelforma-e. A tulajdonságok stimmelnek, és ha gondolatban elvégzem a számolásokat, valóban ugyanazt kapom, mit ahogy az inputban volt
* Tehát megtanulhattuk azt, hogy az operandust leírom, a műveletet berakom, de előtte minden nála nagyobb, illetve balra kötő műveletek esetén minden vele azonos precedenciájú műveleti jelet is kiírok (értelemszerűen, ha nem üres a verem; ha üres, csak berakom)
* A következő példában zárójel is van: a * ( b + c / d ) ^ 2
	* a-t leírom
	* \* verembe
	* ( verembe - tekinthetünk rá úgy, mint egy "verem a veremben" megnyitására, vagy úgy, mint egy operátorra, ami mindenkinél erősebb
	* b-t leírom
	* \+ verembe, a nyitó zárójel "eltakarja" az előző műveleti jelet, ilyenkor úgy tekintsünk rá, mintha üres verembe raknánk az aktuális műveleti jelet (verem a veremben), vagy ha midenkinél gyengébb operátor lenne
	* c-t leírom
	* / verembe, mert nagyobb a precedenciája a veremtetőnél (+)
	* d-t leírom
	* ) triggereli a verem ürítését és kiírását egészen (-ig, mindenféle precedenciák és egyebek figyelembe vétele nélkül. A nyitó zárójel is kikerül, de azt nem írjuk ki (hát persze, hiszen erről szól a lengyelforma) és a csukó zárójel se kerül be, csak eldobjuk
	* ^ verembe, mivel erősebb, mint a veremtető, ami most a szorzás (de ez a jobbasszociativitás miatt akkor is bekerült volna előtte való kiírás nélkül, ha hatványjel van a verem tetején)
	* 2-t leírom
	* vége, elfogyott a bemenet, kikerül sorban ami a veremben van: a hatvány- majd a szorzás jel
	* Eredmény: a b c d / + 2 ^ *
* Az algoritmus előfeltétele a helyes (de nem feltétlen teljes) zárójelezettség, valamint a már a zárójeles résznél emlegetett "prec" függvény megléte
	
```
toRpn(in : InStream, &out : OutStream)
	s : Stack
	ch := in.read()
	AMÍG ch != EOF
		HA ch = '('
			s.push(ch)
		KÜLÖNBEN HA ch eleme OPERAND
			out.print(ch)
		KÜLÖNBEN HA ch = ')'
			AMÍG s.top() != '('
				out.print(s.pop())
			s.pop()
		KÜLÖNBEN HA ch eleme OPERATOR
			HA ch eleme LEFT_ASSOCIATIVE
				AMÍG !s.isEmpty() és s.top() != '(' és prec(s.top()) >= prec(ch)
					out.print(s.pop())
				s.push(ch)
			KÜLÖNBEN HA ch eleme RIGHT_ASSOCIATIVE
				AMÍG !s.isEmpty() és s.top() != '(' és prec(s.top()) > prec(ch)
					out.print(s.pop())
				s.push(ch)
		ch := in.read()
	AMÍG !s.isEmpty()
		out.print(s.pop())
```

* A csukó zárójel esetében, egyrészt nyugodtan ellenőrizhetjük az s.top()-ot az üresség ellenőrzése nélkül, hiszen feltételezve a helyes inputot, kellett már beolvasott nyitó zárójelnek lennie, tehát a verem biztos nem üres. Az a plusz veremből-művelethívás pedig a nyitó zárójel eldobása
* A műveletek esetében a két belső ciklus között a nagyobb jel élessége a különbség: azt írjuk ki hamarabb, amelyiket hamarabb kell elvégezni, tehát jobbasszociatív esetben a jobb oldalit, ezért a már bennlevő baloldali jelet nem dobjuk ki ilyenkor, viszont mindenkit, aki nála erősebb, igen

## Példa a lengyelformára hozó algoritmus lejátszásra

* Legyen az input ez: y := ( b - 2 * h ^ 2 ^ 3 / e ) / j * a + 2 - 1 / w
* Olvassuk be a bemenetet karakterenként, alakítsuk lengyelformára, minden lépésben mutassuk az output és a verem tartalmát is
	* Beolvassuk: y
		* s = [
		* out = y
	* Beolvassuk: :=
		* s = [ :=
		* out = y
	* Beolvassuk: (
		* s = [ := (
		* out = y
	* Beolvassuk: b
		* s = [ := (
		* out = y b
	* Beolvassuk: -
		* s = [ := ( -
		* out = y b
	* Beolvassuk: 2
		* s = [ := ( -
		* out = y b	2
	* Beolvassuk: *
		* s = [ := ( - *
		* out = y b	2
	* Beolvassuk: h
		* s = [ := ( - *
		* out = y b	2 h
	* Beolvassuk: ^
		* s = [ := ( - * ^
		* out = y b	2 h
	* Beolvassuk: 2
		* s = [ := ( - * ^
		* out = y b	2 h 2
	* Beolvassuk: ^
		* s = [ := ( - * ^ ^
		* out = y b	2 h 2
	* Beolvassuk: 3
		* s = [ := ( - * ^ ^
		* out = y b	2 h 2 3
	* Beolvassuk: /
		* s = [ := ( - /
		* out = y b	2 h 2 3 ^ ^ *
	* Beolvassuk: e
		* s = [ := ( - /
		* out = y b	2 h 2 3 ^ ^ * e
	* Beolvassuk: )
		* s = [ :=
		* out = y b	2 h 2 3 ^ ^ * e / -
	* Beolvassuk: /
		* s = [ := /
		* out = y b	2 h 2 3 ^ ^ * e / -
	* Beolvassuk: j
		* s = [ := /
		* out = y b	2 h 2 3 ^ ^ * e / - j
	* Beolvassuk: *
		* s = [ := *
		* out = y b	2 h 2 3 ^ ^ * e / - j /
	* Beolvassuk: a
		* s = [ := *
		* out = y b	2 h 2 3 ^ ^ * e / - j / a
	* Beolvassuk: +
		* s = [ := +
		* out = y b	2 h 2 3 ^ ^ * e / - j / a *
	* Beolvassuk: 2
		* s = [ := +
		* out = y b	2 h 2 3 ^ ^ * e / - j / a * 2
	* Beolvassuk: -
		* s = [ := -
		* out = y b	2 h 2 3 ^ ^ * e / - j / a * 2 +
	* Beolvassuk: 1
		* s = [ := -
		* out = y b	2 h 2 3 ^ ^ * e / - j / a * 2 + 1
	* Beolvassuk: /
		* s = [ := - /
		* out = y b	2 h 2 3 ^ ^ * e / - j / a * 2 + 1
	* Beolvassuk: w
		* s = [ := - /
		* out = y b	2 h 2 3 ^ ^ * e / - j / a * 2 + 1 w
	* Vége, kiírjuk a vermet
		* s = [
		* out = y b	2 h 2 3 ^ ^ * e / - j / a * 2 + 1 w / - :=
