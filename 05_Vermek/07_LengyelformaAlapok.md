# Lengyelforma

* Egy műveletet leírhatunk úgy is, hogy a műveleti jel a két operandus között van (ez a megszokott alak), de akár másképp is:
	* a+b (infix forma)
	* +ab (prefix forma)
	* ab+ (postfix forma)
* A prefix alakot szokás "lengyelformának" (PN - Polish Notation, Jan Łukasiewicz után)
* A postfixes jelölés pedig a fordított lengyelforma (RPN - Reverse Polish Notation, az amúgy ausztrál Charles Leonard Hamblin találmánya)
* Ezen két jelölésnek számtalan haszna van, de mivel géppel az utóbbit könnyebb kiértékelni, ezt fogjuk használni, és mostantól eleve ha a "lengyelforma" szót halljuk, a "fordított lengyelformára" gondolunk!

## Tulajdonságai

* Nincs benne zárójel (erre megy ki a játék eleve)
* Nem kell foglalkozni a precedenciával és asszociativitással
* Az operandusok egymáshoz képest fizikailag abban a sorrendben vannak leírva, mint az infix változatban
* Az operátorok a kiértékelés sorrendjében vannak leírva, ami eltérhet az eredeti sorrendtől az ottani zárójelek, precedencia és az asszociativitás iránya miatt
* Az operátor mindig a két paramétere után van közvetlenül (persze a paraméterek lehetnek egy részkifejezés eredményei)
* Veremmel hozható létre az infix alakból - egy menettel, egyértelmű módon
* Veremmel kiértékelhető - egy menettel, egyértelmű módon

## Bevezető példák

* a * b + c ~ a b * c +
	* Előbb az a * b -t számoljuk ki, majd az "a * b" + c -t, mégpedig a precedenia miatt
* a + b - c ~ a b + c -
	* Ez ugyanolyan lett, itt a precedencia ugyan azonos, de az asszociativitás miatt itt is balról jobbra haladunk
* a + b * c ~ a b c * +
	* A szorzás az erősebb, hiába van "jobbrább", ezért azt írjuk le előbb - tehát ez is a precedencia miatt van így. A "b c *" lesz az összeadás baloldali operandusa. Itt konkrétan megváltozott az operátorok sorrendje - ez lehetséges, az operandusoké nem
* a ^ b ^ c ~ a b c ^ ^
	* Itt az infix "második" hatványjele van elsőnek írva a postfix jelölésben, mivel a jobbasszociativitás miatt azt értékeljük ki hamarabb
* a := b := c ~ a b c := :=
	* Ugyanaz a logika, itt is a jobbasszociativitás a lúdas. Egyébként érdemes a kifejezés jelentését is végiggondolni: ha c pl. 5, akkor b := c értékül adja b-nek is az 5-öt, majd visszatér b új értékével, azaz 5-tel. Ezt kapja az a := (b := c), azaz a is 5 lesz, majd ez az egész kifejezés visszatér azzal, hogy 5
* ( a ^ b ) ^ c ~ a b ^ c ^
	* Itt a zárójelezés miatt marad a hatványjelek eredeti sorrendje
* a * ( b + c ) ~ a b c + *
	* A zárójel miatt előbb a + értékelődik ki, ezért van előbb írva, aztán őt követi a szorzás, aminek a jobb paramétere az összeg!
