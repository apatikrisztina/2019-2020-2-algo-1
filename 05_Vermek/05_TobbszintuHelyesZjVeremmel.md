# Helyes zárójelezés

## Hierarchikus eset - veremmel

* Most nézzük a több szintű zárójelekkel rendelkező esetet
    * A legerősebb a "{ }" zárójel, ebbe kerülhet önmaga és a gyengébbek
    * A következő a "[ ]" zárójel, szintén önmaga és a gyengébbek mehetnek bele
    * Végül a "( )" típus, ebbe csak önmaga kerülhet
* Persze ez nem egy kőbe vésett dolog, nem "égetjük" bele a kódba a zárójelek karaktereit, ez az egész kívülről konfigurálható ideális esetben
    * Vezessünk be egy prec(ch) függvényt, ami adjon vissza egy számot, ami leírja az adott ch-val jelölt zárójel erőségét. Ez minél nagyobb, annál erősebb a precedencia
	    * Pl: prec('{') = prec('}') = 3; prec('(') = 1
    * Tegyük fel, hogy adott egy OPENING és egy CLOSING halmaz a megfelelő elemekkel (nyitó és csukó zárójelek), konstans lekérdezéssel
	* Esetünkben ezek jelentsék a "külső konfigurációt"
* Most a verembe mentsük el a zárójel karakterét is és az indexet is, mert mindkettőre szükség lesz, illetve a zárójel karakter helyett annak precedenciája is elég lenne, mivel még mindig csak a nyitókat rakjuk be
* A v.top()<sub>1</sub> jelentése legyen a veremtető első komponense, ami itt a zárójel karakter lesz
* A fenti algoritmusból kiindulva így lehet ezt a bonyolultabb feladatot megoldani:
```
checkHierarchicalBalancedParentheses(s : InStream, out& : OutStream) : L
	s : Stack //Stack<Ch,N> egész konkrétan, azaz Ch-N párok vannak a veremben
	out := <>
	i := 0
	ch := s.read() // ch : Ch
	AMÍG ch != EOF
		HA ch eleme OPENING
			HA s.isEmpty() v prec(s.top()₁) >= prec(ch)
				s.push(ch, i)
			KÜLÖNBEN
				return false
		KÜLÖNBEN HA ch eleme CLOSING
			HA !s.isEmpty() és prec(s.top()₁) = prec(ch)
				out.print(s.pop()₂)
				out.print(i)
			KÜLÖNBEN
				return false
		KÜLÖNBEN
		    SKIP
		ch := in.read()
		i := i+1
	return s.isEmpty()
```

* Az algoritmus hasonló, mint előzőleg, íme a különbségek:
    * A Stack generikus típusa
    * Ha nyitó zárójelet olvasunk, még automatice nem rakhatjuk a verembe, hanem csak ha vagy ez az első (nem már becsukott) zárójel, vagy ha nála nagyobb-egyenlő precedenciájú nyitott zárójelben áll
    * Különben hiba van
    * A csukónál pedig csak a legutóbb megnyitott nyitó párja az elfogadható
    * Persze a '(' és ')' karakter ellenőrzése helyett az eleme-elleőrzést használjuk
* Nagyon figyeljünk a lusta kiértékelés és a top/pop megfelelő használatára. Itt ellenőrizzük mindig a nemürességet, és arra is figyelünk, ha egy elemet többször is le akarunk kérni, előbb toppal, majd utána poppal tegyük azt, különben másodikra már elveszne az az adott elem