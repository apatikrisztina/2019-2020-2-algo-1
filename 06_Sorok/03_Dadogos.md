# Dadogós

* Döntsük el egy stringről, hogy dadogós-e (azaz a fele után megismétlődik tartalma)
	* Legyen a bemenet streamként adva, különben nem lenne nehéz dolgunk, ha ismernénk a méretét és tudnánk indexelni
* Biztos, hogy lineáris lesz a műveletigény, és az is biztos, hogy több, mint egyszer kell végigmenni az inputon
    * Mivel stringstream, ezért ez azt jelenti, el kell menteni egy gyűjteménybe - ami egy sor lesz a sorrendtartás miatt
    * Törekedjünk arra, hogy ha már kétszer is végigmegyünk az inputon, minél több feladatot végezzünk el egy-egy menet alatt, hogy ne kelljen harmadjára is végigmenni rajta

	```
	isDoubled(s : InStream) : L
		q1 : Queue
		q2 : Queue
		length := 0
		ch := s.read()
		AMÍG ch != EOF
			q1.add(ch)
			length := length+1
			ch := s.read()
		HA 2|length
			n := length/2
			AMÍG n>0
				q2.add(q1.rem())
				n := n-1
			AMÍG !q1.isEmpty()
				HA q1.rem() != q2.rem()
					return false
			return true
		KÜLÖNBEN
			return false
	```
	
	* Bepakoljuk a string betűit egy sorba; közben számoljuk a hosszát
	* Ha nem páros a hossz, nem dadogós (nem tudjuk félbevágni a középső betűt)
	* Ha páros, a felét (az első felét, egyébként) berakjuk egy másik sorba, majd a két sort karakterenként összehasonlítjuk (hasonló a feladat mint majd a konzultáción szereplő palindromos feladathoz, csak ott még inverziót is kell rakni a stringbe)
		* Fontos, hogy itt feltehetjük a két sor kialakítása miatt, hogy a két sor pont egyszerre fog elfogyni. Ha annyi lenne a feladat, hogy hasonlítsunk össze két sort, ezeket a vizsgálatokat még el kellene végezni!
		* Mert hogy is alakítom ki? Tudom, hogy a string hossza páros és azt hogy a q2-ben az első, a q1-ben a második fele van
	* Üres inputra külön ellenőrizzük le, nincs-e sehol túlindexelés
	    * Arra jutunk, nincs: egyik ciklusba vagy elágazásba se lépünk be feleslegesen