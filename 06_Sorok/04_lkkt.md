# Legkisebb közös többszörös

* Adott két sor, mindkettőben egy egynél nagyobb természetes szám prímtényezős felbontása növekvő sorrendben. Lehetnek azonos elemek egy soron belül és egymás között is. Adjuk meg a legkisebb közös többszöröst hasonló formában. A két sor tartalma változhat a futás alatt
* Például
	* q1 = <2, 2, 5, 7, 7>
	* q2 = <3, 5, 5, 13>
	* lcm = <2, 2, 3, 5, 5, 7, 7, 13>
	* És amúgy: gcd = <5> (legnagyobb közös osztó)
* Ötlet: összefuttatás
	* Két féle alapalakja van, a vagyolós és az éselős alapciklusú
* Az input két "zsák" (multiset), az output is legyen az: a cél, az egyikben és/vagy másikban előforduló elemeket vegyük a közös nagyobbik előfordulással (nem az előfordulások összegével, mert akkor összeszoroznánk a két számot)
* Mi van akkor, ha elvárás, hogy az egyik sor maradjon érintetlen (immutable)?
	* Berakok egy extremális értéket az elején ebbe a sorba, és mindig amikor kiveszek elemet onnan, ugyanazt be is rakom (a végére), az üresség-tesztnél pedig az extremális érték meglétét tesztelem. A végén ezt még kiveszem. Így végülis a bemenet nem lesz konstans, mert a futás közben változik a tartalma, de biztosan ugyanaz lesz a tartalma az elején, mint a végén
* Ez most olyan vagyolós alakú változat lesz, ahol az inputok elfogynak

	```
	lcm(q1 : Queue, q2 : Queue) : Queue
	  lcm : Queue
	  AMÍG !q1.isEmpty() vagy !q2.isEmpty()
	    VHA q2.isEmpty() vagy (!q1.isEmpty() és q1.first() < q2.first())
	      lcm.add(q1.rem())
	    VHA q1.isEmpty() vagy (!q2.isEmpty() és q2.first() < q1.first())
	      lcm.add(q2.rem())
	    VHA !q1.isEmpty() és !q2.isEmpty() és q1.first() = q2.first()
	      lcm.add(q1.rem())
	      q2.rem()
	  return lcm
	```
	
	* OOP tárgyból ismert összefuttatás: addig megyek, amíg legalább az egyik nem üres, megnézem melyik "jön" épp a rendezés szerint, azt feldolgozom. Ha mindkettő, akkor nem felejtem el bár csak egyszer feldolgozni, de a másikból is kivenni - ez az unió alapalakja, és az lkkt is egy uniózás végső soron (persze akár csinálhatnám azt is, hogy ilyenkor mindkettő alapján hozzáadom az eredményhez, de akkor nem uniózás (itt most multisetes uniózás) történne
	    * Az egyes ágak magyarázata: q1 eleme jön a felsorolásban, ha q2 üres. Emellett akkor is, ha egyik sem üres, és q1 első eleme a kisebb. Viszont a rövidzár miatt, ha az q2.isEmpty()-n továbbmentünk, csak azt állíthatjuk bizton, hogy q2 nem üres, de azt nem hogy q1 sem - amit a ciklusfeltétel megengedne. Ezért kell q1 nemüresség-ellenőrzése, és ezért nem kell q2-re ugyanez
	    * Látható, az ágak feltétele az őrfeltételek és a ciklusfeltétel miatt viszonylag bonyolult, cserébe "alacsony" a kód, a ciklus után nem kell már mást csinálni
    * Az lnko-nál metszetet kell számolni, így ahhoz a már láncolt listáknál tanult éselős alak illene jobban, de uniózásra is lehet éselés alakú összefésülést használni (és viszont), ahogy a lenti példán láthatjuk:

	```
	lcm(q1 : Queue, q2 : Queue) : Queue
		lcm : Queue
		AMÍG !q1.isEmpty() és !q2.isEmpty()
			VHA q1.first() < q2.first()
				lcm.add(q1.rem())
			VHA q2.first() < q1.first()
				lcm.add(q2.rem())
			VHA q1.first() = q2.first()
				lcm.add(q1.rem())
				q2.rem()
		AMÍG !q1.isEmpty()
			lcm.add(q1.rem())
		AMÍG !q2.isEmpty()
			lcm.add(q2.rem())
	return lcm
	```

	* Itt most addig megyünk, míg igaz, hogy még mindkettőben van elem - így egyszerűsödnek a belső feltételek is!
	* Cserébe a végén előfordulhat hogy egyikben vagy másikban van még elem (mindkettő nem lehet a ciklusfeltétel miatt), ezt így két egymástól független while-ciklussal oldottam most meg (max. csak az egyik fog belépni). Az biztos, hogy bármelyik ciklusba is lépjünk be, a hozzáadandó elemek nagyobbak lesznek, mint amiket a fő ciklusban adtunk hozzá. Az is lehetséges, hogy egyikbe se lépünk be, mert épp együtt fogyott el a két sor
	* Láthatjuk, ha unió jellegű feladatot végzünk az éselős alakkal, ki kell egészítenünk még egy-két ciklussal, ami végigjárja a kimaradt elemeket