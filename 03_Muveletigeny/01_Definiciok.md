# Műveletigény-elemzés alapjai

## Motiváció

* A polinomszámítási példákban láttunk több a műveletigényeket leíró függvényt is
* Az azonos problémára adott (helyes) megoldások műveletigényeit érdemes lehet összehasonlítani, hiszen ha mindkettő helyes, a gyorsabbat (vagy kevesebb memóriát megevőt) érdemes választani
* Ezért jó lenne bevezetni egy osztályozást ez alapján a műveletigények között

## Definíciók

* Legyen f egy műveletigényt leíró függvény, azaz ℕ-ről (input mérete) ℝ<sub>0</sub><sup>+</sup>-ra képez ("lépésszám" - ami értelemszerűen nem lehet negatív)
* Ekkor: Ο(f) := {g : ℕ->ℝ | van c>0 konstans és n<sub>0</sub> >= 0 küszöbindex, hogy: minden n>=n<sub>0</sub>: g(n) <= c*f(n)}
	* Ha g eleme Ο(f), akkor azt mondjuk f aszimptotikus felső korlátja g-nek
	* "Ο" itt egy nagy görög omikron betű, és "nagy ordó"-nak olvassuk ki (ordó = rend)
	* Szemléletesen: ha adott egy f függvény, aki egy műveletigényt ad meg, olyan g függvényekre mondhatjuk azt, hogy nekik f aszimptotikus felső korlátja, amikre igaz az, hogy "egy idő után" bizonyos gépen futtatva (ezt próbálja visszaadni a c konstans) "olcsóbbak" lesznek, mint az f. f "egy idő után" meredekebb lesz mint az ilyen g függvények. Kis értékeknél mindegy úgyis, hogy ki a nagyobb, hiszen abszolút értelemben úgyis jelentéktelen. De pl. egy gyökfüggvény 1-nél kisebb értékekre speciel nagyobb, mint a négyzetfüggvény, de mégis érezzük, hogy egy négyzetes műveletigényű algoritmus rosszabb, mint egy gyökös
* Kis omikronnal (azaz ο) jelöljük a határozott felső korlátot
	* Kiejtése "kis ordó"
	* A formula ugyanaz, csak g(n) < c*f(n)
* Ω(f) := {g : ℕ->ℝ | van c>0 konstans és n<sub>0</sub> >= 0 küszöbindex, hogy: minden n>=n<sub>0</sub>: g(n) >= c*f(n)}
    * Kiejtése: "nagy omega"
	* "Ha g eleme Ω(f)", akkor g lesz hosszú távon a drágább, mint f, azaz az Ω az Ο "fordítottja"
	* Tehát f aszimptotikus alsó korlátja az Ω(f)-be tartozó függvények milljóinak
* ω (kis omega): határozott alsó korlát
	* Az előzők analógiájára
* Θ(f) := {g : ℕ->ℝ | van c<sub>1</sub>, c<sub>2</sub> > 0 és n<sub>0</sub> >= 0 konstansok, hogy minden n>=n<sub>0</sub>-ra: c<sub>1</sub>*f(n) <= g(n) <= c<sub>2</sub>*f(n)}
	* Kiejtése "théta"
	* Ha g eleme Θ(f), akkor f aszimptotikus éles korlátja g-nek
	* Azaz f-fel megfelelő konstansokkal hosszú távon lehet alulról is és felülről is becsülni g-t , azaz nagyságrendileg együtt maradnak nagy n-ekre is, nem távolodik el az egyik a másiktól a kombinatorikus robbanással
    * f ekkor gyakorlatilag aszimptotikus alsó és felső korlát is