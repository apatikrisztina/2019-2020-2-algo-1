# Műveletigény-elemzés - állítások

## Metszet

* Θ(f) = Ο(f) metszet Ω(f), azaz azok és csak azok éles korlátjai egy függvénynek, amik alsó és felső korlátjai is
    * Bizonyítás az egyik minta zh-ban

## Ekvivalenciareláció

* Jelöljük most f Θ g-vel (is) azt, hogy f eleme Θ(g) /infix jelölés/
* Θ ekvivalenciareláció, amiből következik, hogy a világ függvényei körében be tudunk vezetni egy osztályozást a Thétaság alapján
	* Mitől is ekvivalenciareláció az ekvivalenciareláció?
		* Reflexív: azaz egy függvény önmagával Θ-viszonyban van (azaz minden f-re: f Θ f)
		* Tranzitív: azaz, ha f Θ g, g Θ h, akkor minden további bizonyítás nélkül mondható, hogy f Θ h
		* Szimmetrikus: ha f Θ g, akkor g Θ f
	* A későbbiekben még kimondunk néhány tételt és ezek alapján könnyedén besorolhatjuk a függvényeket egy-egy "nevezetes" nagyságrendosztályba, amik között már egyértelműen tudjuk a nagyságrendi különbséget
		* Persze továbbra is érdemes lehet foglalkozni azzal, hogy "n" vagy "n+1" lépést teszünk; esetleg, hogy "c * n" vagy "d * n" lépést; esetleg, hogy a bár átlagos esetben egy algoritmus jobb mint a másik, de szélsőesetben sokkal rosszabb, és pl. az egyenletesség fontosabb szempont, mint az átlagos hatékonyság, de bizonyos algoritmusokat ránézésre ki tudunk majd zárni mások előnyére

## Szekvencia

* Adott f és g műveletigényeket leíró függvények. f+g eleme Θ(max{f,g})
* Szemléletesen: szekvencia műveletigénye korrelál a domináns tag műveletigényével; ha egy rendezési feladat után még ki kell írnunk a legnagyobb elemet a konzolra, akkor nyilván ez nagyságrendileg akkora műveletigény, mintha csak rendezni kéne, mert az az egy kiírás már nem oszt, nem szoroz
* Bizonyítás definícióból: c<sub>1</sub> * max{f(n), g(n)} <= f(n)+g(n) <= c<sub>2</sub> * max{f(n), g(n)}
	* Válasszuk c<sub>1</sub>-et 1-nek, c<sub>2</sub>-t 2-nek. Nyilván ha f is és g is pozitív, akkor 1-szer a maximuma nem lesz nagyobb, mint a kettő összege (hiszen az nem más mint a két függvény maximuma plusz a másik függvény, ami nem nagyobb a maximumnál, de pozitív); és 2-szer a maximuma hasonló okokból nem lesz kisebb, mint a kettő összege

## Zártság

* Az összes az előzőekben definiált relációnk zárt az összeadásra és a pozitív konstanssal való szorzásra
* Θ-ra bizonyítás:
	* Összeadás: ha f eleme Θ(h) és g eleme Θ(h), akkor f+g eleme Θ(h)
		* Az előzőből tudjuk, hogy f+g eleme Θ(max{f,g}), de azt is, hogy max{f,g} eleme Θ(h), ez pedig a tranzitív tulajdonság miatt igazolja állításunk
	* Szorzás: ha f eleme Θ(h) és c>0, akkor c*f eleme Θ(h)
		* Írjuk fel a definíciót, már eleve szerepel benne egy konstans szorzó. Állapítsuk meg úgy, hogy ezt a c-t is figyelembe vesszük (értelemszerűen a definícióbeli és az itteni "c" nem ugyanaz)

## Polinomiális műveletigény

* Egy polinommal megadott műveletigény-függvénynél a legnagyobb fok számít. Azaz minden k-ad fokú polinom eleme Θ(n<sup>k</sup>)
* Bizonyítás:
    * Ha minden a<sub>i</sub> együttható pozitív, a szekvencia-tulajdonságból következik, hogy a legnagyobb kitevőshöz igazodik a teljes kifejezés műveletigénye, az pedig a konstans szorzós tulajdonságból jön, hogy az együttható elhagyható (ezeknek előfeltétele a pozitívság)
	* Különben, írjuk fel a definíciót és osszunk le mindent n<sup>k</sup>-val (n az input mérete, k a polinom foka)
	    * Eredeti: c<sub>1</sub> * n<sup>k</sup> <= a<sub>k</sub>n<sup>k</sup> + a<sub>k-1</sub>n<sup>k-1</sup> + ... + a<sub>1</sub>n + a<sub>0</sub> <= c<sub>2</sub> * n<sup>k</sup>
		* Leosztva: c<sub>1</sub> <= a<sub>k</sub> + a<sub>k-1</sub>/n + ... + a<sub>1</sub>/n<sup>k-1</sup> + a<sub>0</sub>/n<sup>k</sup> <= c<sub>2</sub>
			* Nagy n-re az összes 1/n-es tag elmegy 0-ba, de még n=1 esetben épp az együtthatók összegére jönnek ki, hiszen akkor minden n-es szorzó értéke 1. Ezért c<sub>2</sub> lehet az együtthatók abszolútértékösszege (így garantáltan pozitív)
			* c<sub>1</sub>-nek pedig valami a<sub>k</sub> - epszilont válasszunk, ahol persze epszilon nagyobb 0-nál és kisebb a<sub>k</sub>-nál, hogy c<sub>1</sub> nagyobb lehessen, mint 0. Fontos megjegyezni, hogy a<sub>k</sub> biztosan pozitív, mert maga a függvény is pozitív, és ennek szükséges feltétele, hogy a főegyüttható az legyen (0 pedig azért nem lehet, mert akkor nem az lenne a "főegyüttható", hanem a következő)
			* Figyelem, itt n<sub>0</sub>-t nem adtuk meg szám szerint, mert a konkrét példától függ. Viszont a tendencia alapján tudjuk, hogy valahol lesz egy alkalmas küszöbindex

## Logaritmikus műveletigény

* Bármilyen alapú logaritmusfüggvények is aszimptotikusan Θ-viszonyban vannak egymással. Azaz: log<sub>a</sub>(n) eleme Θ(log<sub>b</sub>(n)), és persze az ekvivalenciareláció miatt fordítva is
	* Bizonyítás:
		* Végtelenben vett határértékkel: lim (n->végtelen) log<sub>a</sub>(n)/log<sub>b</sub>(n), ha ez egy konstans lesz (valami, ami nem függ n-től), akkor Θ egymással a két függvény
		* Márpedig konstans lesz, hiszen ha használjuk az "áttérés más alapú logaritmusra" tételt, akkor megkapjuk, hogy log<sub>a</sub>(n) / (log<sub>a</sub>(n)/log<sub>a</sub>(b)), ezt le tudjuk egyszerűsíteni log<sub>a</sub>(n)-nel, ami marad az már nem függ n-től

## Hatványfüggvények

* Igaz-e?
	* n<sup>a</sup> eleme Θ(n<sup>a+epszilon</sup>), ha a, epszilon > 0
		* Nem. Ugyanis: lim (n->végtelen) n<sup>a</sup> / n<sup>a+epszilon</sup> = lim n<sup>a</sup> / (n<sup>a</sup>*n<sup>epszilon</sup>) = lim n<sup>a</sup>/n<sup>a</sup> * 1/n<sup>epszilon</sup> = 1 * 0 = 0
		* Tehát nem igaz, hanem az n<sup>a</sup> eleme ο(n<sup>a+epszilon</sup>) viszony igaz, azaz egy nagyobb kitevőjű hatvány nagyságrendileg gyorsabban nő egy kisebbnél
	* a<sup>n</sup> eleme Θ(a<sup>n+epszilon</sup>), ha a, epszilon > 0
		* Igaz. Ugyanis: lim (n->végtelen) a<sup>n</sup> / a<sup>n+epszilon</sup> = lim a<sup>n</sup> / (a<sup>n</sup>*a<sup>epszilon</sup>) = lim 1 * 1/a<sup>epszilon</sup>, ami egy n-t nem tartalmazó konstans

