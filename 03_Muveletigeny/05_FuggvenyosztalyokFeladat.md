# Függvényosztályok

## Feladat

* Rendezzük sorba az alábbiakat aszimptotikusan:
	* 5*n<sup>0,001</sup> + 100
	* log<sub>2</sub>(n)
	* n<sup>0,02</sup> – 10
	* (n+1)*log<sub>2</sub>(n)
	* ln(n<sup>2</sup>)
	* 2<sup>n</sup> + 1
	* 0,1 * n<sup>1,01</sup>
	* 2<sup>n+1</sup> - 100
* A megoldáshoz előbb a fentiek sorrendjében a közös alakra hozom a függvényeket:
	* **Θ(n<sup>0,001</sup>)**, mert a konstans szorzó elhagyható, valamint a domináns tag számít, a 100 pedig eleme Θ(1), azaz nem domináns
	* **Θ(log(n))**, így jelöljük a "közös" logaritmusfüggvényt, mert az alapja mindegy
	* **Θ(n<sup>0,02</sup>)**
	* **Θ(n * log(n))**, ugyanis ez n * log(n) + log(n), innen pedig a nagyságrendek szekvenciája tétel él
	* **Θ(log(n))**, mert kiemelhető a kitevő: 2 * log(n), ami így már konstans szorzóként elhagyható 
	* **Θ(2<sup>n</sup>)**
	* **Θ(n<sup>1,01</sup>)**
	* **Θ(2<sup>n</sup>)**, hiszen kiemelhető konstans szorzóként az a plusz egy kettes szorzótényező
* Akkor most rendezzük növekvő sorba az eredeti függvényeket:
	* log<sub>2</sub>(n) ~ ln(n<sup>2</sup>) ≺ 5 * n<sup>0,001</sup> + 100 ≺ n<sup>0,02</sup> - 10 ≺ (itt lennének a lineárisak) ≺ n+1 * (log<sub>2</sub>(n)) ≺ 0,1 * n<sup>1,01</sup> ≺ 2<sup>n</sup> + 1 ~ 2<sup>n+1</sup> – 100
