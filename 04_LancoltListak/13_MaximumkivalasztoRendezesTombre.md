# Összehasonlító rendezések - Maximumkiválasztó rendezés

## Tömbös változat

* Kis kitérő, hogy jobban megértsük a láncolt megvalósítást:
    * Vizsgára kell!

```
maximumSelectionSort(a : T[n])
  i := 1 to n-1
    max := a[0]
    ind := 0
    j := 1 to n-i
      HA a[j] > max
        max := a[j]
        ind := j
    a[ind] := a[n-i]
    a[n-i] := max
```

* Az algoritmus alapja, hogy összesen n-1 alkalommal lefuttassuk a maximumkiválasztást a tömb elejére, majd a maximum elemet a végére pakoljuk
	* Első alkalommal futtassuk tehát le az egész tömbre, majd cseréljük ki az utolsó elemmel a kapott maximumot
	* Második körben csak [0..n-2]-ig menjünk, majd az n-2. elemmel cseréljük ki, hiszen ez már csak az egész, eredeti tömbben globálisan a második legnagyobb elem lehet
	* Így haladhatunk, végül marad egy elemű tömbprefixünk, ami gyakorlatilag annyit jelent, hogy a 0. és 1. indexű elemeket megcseréljük, ha szükséges
	* Ezekről tudjuk, hogy ők a globális legkisebb két elem, mert ha nem így lett volna, már korábban kiválasztódtak volna. Ezért tehát ez az eljárás helyes
* Az algoritmus ciklusmagjának elején kijelöljük az első elemet maxnak, majd a többi elemmel hasonlítjuk ezt össze (azért megyünk 0 helyett 1-től a ciklusban)
	* Az egyből szemet szúrhat, hogy csak úgy meg mertük címezni a[0]-t. És mi van, ha a tömb üres? Maximumkiválasztás programozási tételnél ez mindig neuralgikus pont. Mivel 1..n-1-ig megy a külső ciklus, amibe márpedig beléptünk, n=0, de még csak n=1 esetén se tehettük volna ezt meg (és nagyon helyes is így, egy nulla vagy egy elemű tömb gyárilag rendezettnek tekinthető)
* Invariánsként felírható ind és max viszonya: max a még vizsgált prefix eddig felsorolt elemei közötti legnagyobb, ind ennek indexe - ehhez méltón kell frissíteni ezt
* A ciklus felső határa úgy jött ki, hogy tudjuk, minden körben egyre nőnie kell a rendezett suffixnek, de ez kezdetben még üres
	* Természetesen ahogy növeljük i-t, így fog nőni ez a rendezett rész, ezért van, hogy pont a belső ciklus felső határával cseréljük ki a maximális elemet
* A műveletigényről: a maximumkiválasztásról tudjuk, hogy Θ(n)-es, mivel az egy programozási tétel. Ezt hajtjuk végre n-1-szer, ezért ez Θ(n<sup>2</sup>)
* Érdekességképpen, 1-től indexelt tömbre is közlöm a rendezést:

```
maximumSelectionSort(a/1 : T[n])
  i := 1 to n-1
    max := a[1]
    ind := 1
    j := 2 to n-i+1
      HA a[j] > max
        max := a[j]
        ind := j
    a[ind] := a[n-i+1]
    a[n-i+1] := max
```
