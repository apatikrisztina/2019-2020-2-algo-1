# Beszúrás egyszerű listába

* Három különböző esetnek van értelme:
	* Beszúrás a lista elejére (ez jó, mert konstans a műveletigénye)
	* Beszúrás a végére (lineáris műveletigény - nem feltétlen van nagy haszna)
	* Rendezett listába való beszúrás a rendezettség fenntartásával
		* Ennek az alábbi esetei vannak:
			* Üres lista
			* Lista elejére
			* Lista közepébe
			* Lista végébe
		* Ha a rendezett beszúrás algoritmusát írjuk, erre a 4 esetre érdemes tesztelni

## Beszúrás lista elejére

* l-t referencia szerint kell átadnunk, mert maga l értéke (ne feledjük, ez egy szám - egy memóriacím) változik

	```
	insert(&l : E1*, k : T)
	    HA l == NULL
	        p := new E1
	        p->key := k
	        l := p
        KÜLÖNBEN
            p := new E1
            p->key := k
            p->next := l
            l := p
	```

* A két eset csak egy sorban különbözik. Tudjuk, az E1 esetében a next NULL-ra inicializálódik. Épp ezért az l == NULL ágon egy p->next := l, azaz NULL értékadás nem osztana-szorozna semmit
* Ezért az algoritmus egyszerűsíthető
* Nagyságrendi javulást nem okoz a hatékonyságban, sőt az l == NULL ágon ez egy árnyalatnyit rosszabb, mert az értékadás drágább, mint a NULL-vizsgálat (a másik ágon meg emiatt egy picit olcsóbb), de ez annyira jelentéktelen különbség, hogy az alábbi egyszerűsítést a kód olvashatósága miatt mindenképp érdemes megejteni:

	```
	insert(&l : E1*, k : T)
	    p := new E1
	    p->key := k
	    p->next := l
	    l := p
	```
* Ha lenne olyan konstruktora E1-nek ami egy kulcsot és egy next pointert vár, akkor így is megoldható lenne a dolog:

	```
	insert(&l : E1*, k : T)
	    l := new E1(k, l)
	```

* Ehhez azt kell megérteni, hogy a modellünkben (mint amúgy a programozási nyelvekben is) az értékadás jobbról balra értékelődik ki - azaz, előbb adódik át l régi értéke a konstruktornak (ahol beállítódik az új elem nextjének), majd utána íródik felül a már létrejött új elemre
* Az emlegetett konstruktor pedig így néz ki:

	```
	E1::E1(k : T, n : E1*)
	    key := k
	    next := n
	```

* A két kettőspont előtti E1 a típus neve, ez a "minősítés" azt jelenti, hogy az adott, E1 nevű típus egy metódusát írjuk meg, azaz a függvény nem a "vakvilágba" fut, hanem egy E1 típusú objektumon hívható meg
    * Például, ha adott egy T::f() függvény, akkor ennek a kódnak van érteme:
    
	```
	    pelda(t : T)
	        p := new T
	        t.f()
	        p->f()
	```
	
	* A p csak azért került bele, hogy legyen példa pointeren keresztüli meghívásra is, ami mint látjuk ugyanolyan, mint ha adattagokat érünk el
* Az E1 típusban adott E1 nevű függvény egy speciális metódus, bármilyen típuson a típus nevével megegyező függvényt konstruktornak hívunk
    * Több ilyen nevű függvény is lehet: ez úgy lehet, ha a paraméterezéseik különböznek (ezt hívjuk konstruktor-túlterhelésnek - overload)
        * Disclaimer: minden függvény túlterhelhető
    * Ha egy típushoz nincs konstruktor, akkor implicite generálódik egy paraméter nélküli, ami nem csinál semmit
        * Ha akármilyen konstruktort írunk, nincs implicit konstruktor, még akkor se, ha amúgy nem olyat adtunk meg
        * A paraméter nélküli konstruktor másik neve "default konstruktor" (alapértelmezett konstruktor)
    * A konstruktor neve csalóka, mert ez nem az a függvény, ami konstruálja, létrehozza az E1 típusú elemet, hanem ez az a metódus, ami a valaki által (pl. a new parancs által) létrejövő elem konstruálásának részeképp lefut, tehát a "létrehozó" a hívó közeg, a konstruktor pedig az eszköze a létrehozásnak, vagyis konkrétan az inicializálásnak (kezdeti értékadásnak, ami egyebek mellett a típusinvariánst kielégíti)
* Az E1 típusbeli bármilyen metódus "látja" az E1 típus privát változóit és metódusait, és csak "simán" meg tudják ezeket címezni/hívni
    * Erre egy példa a key := k értékadás. A k a paraméter, a key pedig annak az E1-nek a key adattagja, akin a konstruktor meg van hívva. Ez a "this" vagy "self" a programozási nyelvekben. Innen is látszik, hogy a konstruktor lefutásakor az objektum már "létezik", csak még inicializálatlan
* A konstruktorok általában publikus láthatóságúak
* Vissza az eredeti változathoz: ez ilyen irányban is egyszerűsíthető lenne, az l == NULL ágon nem is kell megőrizni l értékét:

```
insert(&l : E1*, k : T)
    HA l == NULL
        l := new E1
        l->key := k
    KÜLÖNBEN
        p := new E1
        p->key := k
        p->next := l
        l := p
```
