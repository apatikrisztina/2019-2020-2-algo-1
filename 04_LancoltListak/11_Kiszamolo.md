# Kiszámoló
	
* Legyen adott n darab gyerek egy listában, akik egy k szótagú mondókával játszanak kiszámolót
* Az első gyerekkel kezdünk
* Minden k-adik szótagra az a gyerek, akire épp mutatunk, kiáll a csapatból
* Írjuk ki a gyerekek neveit a kiszámolásuk sorrendjében
* Példa: n=3, k=2, a gyerekek "nevei" legyenek a sorszámaik
	* Első kör: <1, 2> - a 2. gyereknél ér véget a mondóka, azaz kiírjuk: 2
	* Marad 1, 3 és 3 jön, azaz <3, 1> a következő kör, kiírjuk: 1
	* Marad 3, <3, 3> a kiszámoló, kiírjuk: 3
		* A végső sorrend: <2,1,3>

	```
	kiszámoló(f : E2C*, k : N)
		p := f
		AMÍG p->next != p
			i := 1 to k
				p := p->next
				HA p == f
					p := p->next
			print(p->key)
			p->prev->next := p->next
			p->next->prev := p->prev
			s := p
			p := p->prev
			delete s
	```

* Ezt egy H2CL-val oldjuk meg
* Elindulunk a fejelemtől
* Addig kell mennünk, amíg nem igaz az, hogy a mutató rákövetkezője saját maga: ez csak akkor történhet meg, ha már csak a fejelem van (mert az mindenképp van), azaz amikor a lista üresnek mondható
* k-szor számolunk, szótagonként léptetjük a listát - ha a fejelemet érintjük, azt még pluszban átugorjuk
	* i=1 esetben lépünk a fejelemről a következőre, tehát pontosan az i. körre léptünk i szótagot
* A belső ciklus után elértük az épp kiszámolt gyereket, kiírjuk, majd kifűzzük
	* Előbb az előzőjének a következőjét átmutattatjuk az ő eddigi következőjére
	* Majd a következőjének az előzőjét visszük át az ő eddigi előzőjére (NULL-okkal itt most nem kell törődnünk - nem ciklikus esetben le kell védeni)
* Utána töröljük az elemet a listából
	* Ehhez előbb kimentjük p-t egy változóba, majd p-t "csökkentjük", mégpedig azért, hogy amikor újra számolni kezdünk a p utáni gyerek legyen az első, akire számolunk. Mivel p előzőjének a következője immáron az, aki p következője, ezzel nem lesz gond
	    * Ha belegondolunk épp ezzel a logikával lett f-re inicializálva p
	* Végül mivel p már biztonságos elemre mutat, a kimentett törlendő elemet valóban törölhetjük is
* Még megér egy kérdést, hogy mi lesz akkor, amikor már csak egy elem van
	* Igazából ekkor két elem van, hiszen a fejelem is ott van, simán végigmegyünk ezen a listán is, és akkor fogunk terminálni, amikor csak a fejelem van a listában. Tehát az utolsó gyereket is ugyanúgy kiszámolja, kiírja, kifűzi
