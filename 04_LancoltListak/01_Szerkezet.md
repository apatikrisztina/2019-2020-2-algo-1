# Bevezetés a láncolt listákhoz

## Pointerek, alapszerkezet

* A láncolt lista iterált, lineáris és rekurzív adatszerkezet
    * **Iterált**, mert egyazon alaptípus (ami persze nem szükségképpen primitív, sőt láncolt lista is lehet) felett értelmezett, csupa azonos típusú elemeket tartalmaz
    * **Lineáris**, mert jól definiálható sorrendben végig lehet menni az elemein, azaz kvázi indexelhető (ez a rendezhetőségtől független fogalom)
    * **Rekurzív**, mert a típus definíciója önmagára hivatkozik
* A tömbbel ellentétben, nincs kőbe vésett mérete, így egyrészt kis kihasználtság esetén nem pazarló, másrészt potenciálisan végtelen kapacitású - ennyit az előnyeiről
* Egy listát mindig egy mutatóval (pointer) adunk meg, ami saját maga a memória statikus részén (stack), de az a változó amire mutat, azaz aminek a memóriacímét tartalmazza, mint érték a memória dinamikus részén (heap) foglal helyet
* Egy ilyen változó típusát E1-nek (illetve később E2-nek és E2C-nek) fogjuk hívni. Az E1-re mutató mutató típusa E1* (minden mutató típusos, és mindig az adott típus neve + egy csillag a mutató típusának neve)
* A listaelemeket angolul hagyományosan Node-nak hívják, néha így hívom magam is
* Egy ilyen dinamikus helyfoglalású változó nem jön létre, és nem is törlődik magától, amikor egy pointert deklarálunk, behozunk vagy kiviszünk a scope-ból. Maga a pointer egy primitív változó, ami gyakorlatilag egy intet tartalmaz..., ez kezdetben értéket is kap, de valami definiálatlan, ha úgy tetszik veszélyes, de legalábbis értelmetlen helyre mutat, de bármikor át lehet "mutattatni" egy már meglévő másik pointer által mutatott memóriaterületre
	* Ha van egy p és egy q pointerem, akkor a p := q értékadás annyit fog jelenteni, hogy mostantól a p is oda mutat, ahova a q. Azt is mondhatjuk, a q által mutatott területnek lett egy új álneve (alias), ami a p. Ha a p eddig mutatott valahova, és azt a helyet semmilyen más módon nem címeztük meg (nem mutat rá direktben vagy közvetetten másik pointer), akkor ilyenkor azt a területet végérvényesen elvesztettük, de az operációs rendszer szempontjából ugyanúgy lefoglalva marad (memóriaszivárgás). Ez mindenképpen hibának számít
* Egy p pointer által mutatott területet felszabadítani, törölni, az operációs rendszernek visszaadni a "delete p" paranccsal lehet. Ezután maga a p pointer továbbra is létezni fog (a szülő blokk végéig), de értéke definiálatlan lesz
* A statikusan létrejövő változókat (pl. a pointert magát) "törölni" nem lehet
* A dinamikus memórián új területet foglalni, annak értéket adni pedig a "p := new E1"-gyel lehet. Ekkor valahol az operációs rendszer lefoglal egy "E1-nyi" területet, arra lefuttatja az E1 típus konstruktorát, majd visszatér a memóriacímmel és azt p-nek adja. p ezutántól egy "nyíl" lesz, ami egy névtelen, de kétségtelenül lefoglalt és p segítségével elérhető memóriacellá(k)ra mutat
* Egy pointernél előfordulhat, hogy "szándékosan" sehova se mutat. Ezt az értéket NULL-nak szoktuk nevezni. Egy pointer deklarálásakor ebben a modellben nem NULL-ra inicializálódik, hanem definiálatlan értéke lesz - tehát az ilyen irányú kezdeti értékadásokat explicit ki kell adni
* Az E1 típus - mint majd láthatjuk - egy rekord. Ha van egy e nevű E1 típusú változóm, akkor ahogy korábban megszokhattuk, a "key" mezőjét a megfelelő szelektorfüggvénnyel érhetjük el, ami az "e.key". Ha p egy E1* típusú pointer, akkor nyilván nincs "p.key", mert a pointer nem egy E1, hanem egy szám..., de a p által mutatott E1 típusú elem kulcsát mégiscsak el tudjuk érni, mégpedig így: "p->key"
* A pointerek által mutatott rekordok mezőinek elérését lehet láncolni, azaz írhatunk ilyet: "p->next->key", és azt értjük alatta, hogy "annak a node-nak, amire a p mutat, a rákövetkezőjének a kulcsa". Viszont biztonságosan csak akkor alkalmazhatjuk az ilyen jelöléseket, ha tudjuk, hogy p, illetve p->next nem NULL
* Mint jeleztem, ha adott egy p pointer, a p.adat kifejezésnek nincs értelme, mivel a pointer csak egy szám, nem pedig "Adat". Jelölhetjük a "p pointer által mutatott értéket" *p-vel, ekkor a (*p).adat már értelmes lesz, ezt hívjuk dereferálásnak, és ennek a rövidítése a p->adat
* A nullpointer is egy "értelmes" érték, viszont az adattagjaira nem hivatkozhatunk. Igazából addig nem hivatkozhatunk, amíg nem tudjuk, hogy 1) a pointer nem definiálatlan vagy NULL, 2) az adattagok nem inicializálatlanok

## Az E1 típus

* A láncolt lista egy eleme két, úgymond kötelező részből áll, ez a "key" és a "next"
	* A kulcsra úgy tekintünk, mint az adott listaelem azonosítójára. Így szerencsés esetben ez egyedi. De mivel ezt a tulajdonságot nem feltétlenül használjuk mindig ki, ezért hacsak kifejezetten nem mondjuk ezt ki, alapból nem feltételezzük. Azt viszont igen, hogy összehasonlítható, rendezhető, viszonylag egyszerű felépítésű, az összehasonlítás gyors rajta. A gyakorlatban számokat vagy betűket fogunk alkalmazni - hiszen egyáltalán nem fontos, hogy ezek a listák pontosan miket is tartalmaznak, az elv a lényeg, ahogy egy listát kezelni tudunk
	* A next pedig egy E1*, ő lesz a lista következő elemére mutató pointer. A lista végét nullpointerrel szoktuk jelölni
	* Természetesen az E1-nek lehetnek más részei is, sőt a gyakorlatban biztosan vannak is: maga az "adat" (payload), amit az elem tárol, de ez a konkrét feladattól függ, az általános listaműveletek szempontjából indifferens, ezért az E1-eket hacsak mást nem mondunk mindig a (key,next) párral adjuk meg, és hacsak mást nem mondunk, nem másolhatjuk le, mivel nem feltétlen ismerjük a copy constructorát és annak futásidejéről sem tudunk állításokat tenni - ahol tudjuk, "átláncolással" oldjuk meg a listaelemek (tartalmának) cseréjét, törlését, stb-jét
* UML-diagramban így írnánk:
    ```
    + key : T
    + next : E1*
    ```
* Azaz ezeket az adattagokat elérjük direktben kívülről is
* Az E1 konstruktora a nextet NULL-ra inicializálja, a key kezdőértéke viszont definiálatlan
    * Definiálatlan értéket nem kérdezhetünk le (amúgy ez a Java nyelvben van így, konkrétan fordítási hibát okoz egy (potenciálisan) inicializálatlan lokális változó rvalue-ként (azaz olvasásra való) használata
* Fentebb írtam a láncolt ábrázolásból adódó előnyökről a tömbös ábrázolással szemben, most lássuk a hátrányokat:
    * A tömbös ábrázoláshoz képest egy n méretű lista nem n * elemméret tárhelyet foglal, elemenként van egy kis overhead
    * Ugyan egyesével bejárni ugyanúgy lineáris idő, mint a tömb esetén, de egy specifikus i-edik indexen levő elemet kiválasztani (ahol i eleme [1..n]), a next mentén való i-1 ugrásból áll, azaz az indexelés nem konstans, hanem lineáris műveletigényű művelet. Ezért gyakorlatilag sosem használjuk, mint írtam, a listabejárás viszont olcsó, ezt tehát gyakran fogjuk használni. A tömb viszont jobban használható a "random access" elérésre
* A láncolt lista egy elemének key mezőjének típusa, ahogy írtuk "T". Ez egy általános, közelebbről nem specifikált típus. Használhatjuk akár a generikus paraméteres jelölést, azaz E1\<S>, ekkor key : S és next : (E1\<S>)* lesz igaz. Viszont ezt a jelölést felesleges lenne végigvinni egyértelmű esetekben, ezért nem is szoktuk, mindenhova "odaértjük" a \<T>-t
* Az eddigiekből következik, de nem árt leszögezni, hogy tulajdonképpen (modellünkben) láncolt lista típus nem létezik, csak az elemeinek típusa, magát azt a változót, amit szemléletesen "a" láncolt listának gondolunk, az első elemére mutató E1* típusú változóval adjuk meg, konvencionálisan ennek a neve l